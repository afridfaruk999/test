﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074;
// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// up_manager
struct up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t8EA72E90B3BD1392FB3B3EF167D5121C23569E4C;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;

IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94;
IL2CPP_EXTERN_C String_t* _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1;
IL2CPP_EXTERN_C String_t* _stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986;
IL2CPP_EXTERN_C String_t* _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF;
IL2CPP_EXTERN_C String_t* _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55;
IL2CPP_EXTERN_C String_t* _stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355;
IL2CPP_EXTERN_C String_t* _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800;
IL2CPP_EXTERN_C String_t* _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2;
IL2CPP_EXTERN_C String_t* _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F;
IL2CPP_EXTERN_C String_t* _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1;
IL2CPP_EXTERN_C String_t* _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43;
IL2CPP_EXTERN_C String_t* _stringLiteral47BD0CAE8B2D419C4141C37C3DB237D0F602AA3E;
IL2CPP_EXTERN_C String_t* _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4;
IL2CPP_EXTERN_C String_t* _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC;
IL2CPP_EXTERN_C String_t* _stringLiteral50863FE64FB84E5522BDDE89CF1B95FBD9FEBDCA;
IL2CPP_EXTERN_C String_t* _stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957;
IL2CPP_EXTERN_C String_t* _stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304;
IL2CPP_EXTERN_C String_t* _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39;
IL2CPP_EXTERN_C String_t* _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98;
IL2CPP_EXTERN_C String_t* _stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74;
IL2CPP_EXTERN_C String_t* _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF;
IL2CPP_EXTERN_C String_t* _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D;
IL2CPP_EXTERN_C String_t* _stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C;
IL2CPP_EXTERN_C String_t* _stringLiteral70D790BE2680205977981BBCD96B3F0E742A057E;
IL2CPP_EXTERN_C String_t* _stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F;
IL2CPP_EXTERN_C String_t* _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309;
IL2CPP_EXTERN_C String_t* _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31;
IL2CPP_EXTERN_C String_t* _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722;
IL2CPP_EXTERN_C String_t* _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470;
IL2CPP_EXTERN_C String_t* _stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC;
IL2CPP_EXTERN_C String_t* _stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD;
IL2CPP_EXTERN_C String_t* _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC;
IL2CPP_EXTERN_C String_t* _stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52;
IL2CPP_EXTERN_C String_t* _stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA;
IL2CPP_EXTERN_C String_t* _stringLiteralABBE9CBE1EFB1B0D5CE746178D6D006A481E03A9;
IL2CPP_EXTERN_C String_t* _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE;
IL2CPP_EXTERN_C String_t* _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7;
IL2CPP_EXTERN_C String_t* _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D;
IL2CPP_EXTERN_C String_t* _stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA;
IL2CPP_EXTERN_C String_t* _stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733;
IL2CPP_EXTERN_C String_t* _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4;
IL2CPP_EXTERN_C String_t* _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50;
IL2CPP_EXTERN_C String_t* _stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5;
IL2CPP_EXTERN_C String_t* _stringLiteralEA7A676433CAB71F816C191BA6205A79CA64BAF8;
IL2CPP_EXTERN_C String_t* _stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF;
IL2CPP_EXTERN_C String_t* _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2;
IL2CPP_EXTERN_C String_t* _stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var;

struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C 
{
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD 
{
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_pinvoke
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_com
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 
{
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// up_manager
struct up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject[] up_manager::lvl_mul
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_mul_4;
	// UnityEngine.UI.Text up_manager::mul_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___mul_txt_5;
	// UnityEngine.UI.Text up_manager::money_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___money_txt_6;
	// UnityEngine.UI.Text up_manager::gems_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___gems_txt_7;
	// System.Int32 up_manager::level
	int32_t ___level_8;
	// System.Int32 up_manager::res_level
	int32_t ___res_level_9;
	// UnityEngine.GameObject[] up_manager::lvl_resis
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_resis_10;
	// UnityEngine.UI.Text up_manager::res_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___res_txt_11;
	// System.Int32 up_manager::coin_level
	int32_t ___coin_level_12;
	// UnityEngine.GameObject[] up_manager::lvl_coin
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_coin_13;
	// UnityEngine.UI.Text up_manager::coin_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___coin_txt_14;
	// System.Int32 up_manager::vel_level
	int32_t ___vel_level_15;
	// UnityEngine.GameObject[] up_manager::lvl_vel
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_vel_16;
	// UnityEngine.UI.Text up_manager::vel_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___vel_txt_17;
	// System.Int32 up_manager::gra_level
	int32_t ___gra_level_18;
	// UnityEngine.GameObject[] up_manager::lvl_gra
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_gra_19;
	// UnityEngine.UI.Text up_manager::gra_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___gra_txt_20;
	// System.Int32 up_manager::scr_lvl
	int32_t ___scr_lvl_21;
	// UnityEngine.GameObject[] up_manager::lvl_scr
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___lvl_scr_22;
	// UnityEngine.UI.Text up_manager::scr_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___scr_txt_23;
	// UnityEngine.UI.Text up_manager::homing_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___homing_txt_24;
	// UnityEngine.UI.Text up_manager::splt_text
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___splt_text_25;
	// UnityEngine.UI.Text up_manager::spike_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___spike_txt_26;
	// UnityEngine.UI.Text up_manager::hyper_txt
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___hyper_txt_27;
	// UnityEngine.GameObject up_manager::buy_panal
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___buy_panal_28;
	// UnityEngine.GameObject up_manager::upgrade_panal
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___upgrade_panal_29;
	// UnityEngine.GameObject up_manager::slider
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___slider_30;
	// UnityEngine.UI.Button up_manager::upgrade
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___upgrade_31;
	// UnityEngine.UI.Button up_manager::buy
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___buy_32;
	// UnityEngine.GameObject up_manager::warningpanal
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___warningpanal_33;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTargetCache
	bool ___m_RaycastTargetCache_11;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_12;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_13;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_14;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_15;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_16;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_19;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_20;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_23;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_24;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_25;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_26;
};

// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074* ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35* ___m_CanvasGroupCache_19;
};

// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t8EA72E90B3BD1392FB3B3EF167D5121C23569E4C* ___m_OnClick_20;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_27;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_28;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_32;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_33;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_34;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_35;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_36;
};

// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite_38;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_OverrideSprite_39;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_40;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_41;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_42;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_43;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_44;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_45;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_46;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_47;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_48;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_49;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_50;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_51;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_37;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_39;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_40;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_42;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_43;
};

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// UnityEngine.Color

// UnityEngine.Color

// UnityEngine.Color32

// UnityEngine.Color32

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.UI.Navigation

// UnityEngine.UI.Navigation

// System.Single

// System.Single

// UnityEngine.UI.SpriteState

// UnityEngine.UI.SpriteState

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Vector4

// System.Void

// System.Void

// UnityEngine.UI.ColorBlock
struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11_StaticFields
{
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___defaultColorBlock_7;
};

// UnityEngine.UI.ColorBlock

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// UnityEngine.Component

// UnityEngine.Component

// UnityEngine.GameObject

// UnityEngine.GameObject

// UnityEngine.Behaviour

// UnityEngine.Behaviour

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// UnityEngine.EventSystems.UIBehaviour

// UnityEngine.EventSystems.UIBehaviour

// up_manager

// up_manager

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_21;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_22;
};

// UnityEngine.UI.Graphic

// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712_StaticFields
{
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
};

// UnityEngine.UI.Selectable

// UnityEngine.UI.Button

// UnityEngine.UI.Button

// UnityEngine.UI.MaskableGraphic

// UnityEngine.UI.MaskableGraphic

// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_ETC1DefaultUI_37;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_VertScratch_52;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_UVScratch_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Xy_54;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Uv_55;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19* ___m_TrackedTexturelessImages_56;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_57;
};

// UnityEngine.UI.Image

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_41;
};

// UnityEngine.UI.Text
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF  : public RuntimeArray
{
	ALIGN_FIELD (8) GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* m_Items[1];

	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668 (String_t* ___0_key, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987 (String_t* ___0_key, const RuntimeMethod* method) ;
// System.Boolean System.Convert::ToBoolean(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102 (int32_t ___0_value, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) ;
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB (String_t* ___0_key, const RuntimeMethod* method) ;
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972 (float* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285 (String_t* ___0_key, float ___1_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948 (String_t* ___0_key, int32_t ___1_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m0957E62F2A0A0243C79394E5B74E8EFA86BE5ED1 (int32_t ___0_sceneBuildIndex, const RuntimeMethod* method) ;
// System.Int32 System.Convert::ToInt32(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_m54E891D837D238AD3700E7554AA565E69A7BC983 (bool ___0_value, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___0_r, uint8_t ___1_g, uint8_t ___2_b, uint8_t ___3_a, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___0_c, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void up_manager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_Start_m40D0F71B8C396AF2A7BA8832639A814804A966BC (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47BD0CAE8B2D419C4141C37C3DB237D0F602AA3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50863FE64FB84E5522BDDE89CF1B95FBD9FEBDCA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70D790BE2680205977981BBCD96B3F0E742A057E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA7A676433CAB71F816C191BA6205A79CA64BAF8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	int32_t V_25 = 0;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	int32_t V_28 = 0;
	int32_t V_29 = 0;
	int32_t V_30 = 0;
	int32_t V_31 = 0;
	int32_t V_32 = 0;
	int32_t V_33 = 0;
	int32_t V_34 = 0;
	int32_t V_35 = 0;
	int32_t V_36 = 0;
	int32_t V_37 = 0;
	int32_t V_38 = 0;
	int32_t V_39 = 0;
	int32_t V_40 = 0;
	int32_t V_41 = 0;
	int32_t V_42 = 0;
	int32_t V_43 = 0;
	int32_t V_44 = 0;
	int32_t V_45 = 0;
	int32_t V_46 = 0;
	int32_t V_47 = 0;
	int32_t V_48 = 0;
	int32_t V_49 = 0;
	int32_t V_50 = 0;
	int32_t V_51 = 0;
	int32_t V_52 = 0;
	int32_t V_53 = 0;
	int32_t V_54 = 0;
	int32_t V_55 = 0;
	int32_t V_56 = 0;
	int32_t V_57 = 0;
	int32_t V_58 = 0;
	int32_t V_59 = 0;
	int32_t V_60 = 0;
	int32_t V_61 = 0;
	int32_t V_62 = 0;
	int32_t V_63 = 0;
	int32_t V_64 = 0;
	int32_t V_65 = 0;
	int32_t V_66 = 0;
	int32_t V_67 = 0;
	int32_t V_68 = 0;
	int32_t V_69 = 0;
	int32_t V_70 = 0;
	int32_t V_71 = 0;
	int32_t V_72 = 0;
	int32_t V_73 = 0;
	int32_t V_74 = 0;
	int32_t V_75 = 0;
	int32_t V_76 = 0;
	int32_t V_77 = 0;
	int32_t V_78 = 0;
	int32_t V_79 = 0;
	int32_t V_80 = 0;
	int32_t V_81 = 0;
	int32_t V_82 = 0;
	int32_t V_83 = 0;
	int32_t V_84 = 0;
	int32_t V_85 = 0;
	int32_t V_86 = 0;
	int32_t V_87 = 0;
	int32_t V_88 = 0;
	int32_t V_89 = 0;
	int32_t V_90 = 0;
	int32_t V_91 = 0;
	int32_t V_92 = 0;
	int32_t V_93 = 0;
	int32_t V_94 = 0;
	int32_t V_95 = 0;
	int32_t V_96 = 0;
	int32_t V_97 = 0;
	int32_t V_98 = 0;
	int32_t V_99 = 0;
	int32_t V_100 = 0;
	int32_t V_101 = 0;
	int32_t V_102 = 0;
	int32_t V_103 = 0;
	int32_t V_104 = 0;
	int32_t V_105 = 0;
	int32_t V_106 = 0;
	int32_t V_107 = 0;
	int32_t V_108 = 0;
	int32_t V_109 = 0;
	int32_t V_110 = 0;
	int32_t V_111 = 0;
	int32_t V_112 = 0;
	int32_t V_113 = 0;
	int32_t V_114 = 0;
	int32_t V_115 = 0;
	int32_t V_116 = 0;
	int32_t V_117 = 0;
	int32_t V_118 = 0;
	int32_t V_119 = 0;
	int32_t V_120 = 0;
	int32_t V_121 = 0;
	int32_t V_122 = 0;
	int32_t V_123 = 0;
	int32_t V_124 = 0;
	{
		// if (!PlayerPrefs.HasKey("pow_lvl"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		// level = 0;
		__this->___level_8 = 0;
		// money_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1 = __this->___money_txt_6;
		NullCheck(L_1);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_0035;
	}

IL_0025:
	{
		// level = PlayerPrefs.GetInt("pow_lvl");
		int32_t L_2;
		L_2 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, NULL);
		__this->___level_8 = L_2;
	}

IL_0035:
	{
		// if (!PlayerPrefs.HasKey("res_lvl"))
		bool L_3;
		L_3 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, NULL);
		if (L_3)
		{
			goto IL_005a;
		}
	}
	{
		// level = 0;
		__this->___level_8 = 0;
		// money_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_4 = __this->___money_txt_6;
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_006a;
	}

IL_005a:
	{
		// level = PlayerPrefs.GetInt("res_lvl");
		int32_t L_5;
		L_5 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, NULL);
		__this->___level_8 = L_5;
	}

IL_006a:
	{
		// if (!PlayerPrefs.HasKey("coin_lvl"))
		bool L_6;
		L_6 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, NULL);
		if (L_6)
		{
			goto IL_008f;
		}
	}
	{
		// coin_level = 0;
		__this->___coin_level_12 = 0;
		// coin_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_7 = __this->___coin_txt_14;
		NullCheck(L_7);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_009f;
	}

IL_008f:
	{
		// coin_level = PlayerPrefs.GetInt("coin_lvl");
		int32_t L_8;
		L_8 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, NULL);
		__this->___coin_level_12 = L_8;
	}

IL_009f:
	{
		// if (!PlayerPrefs.HasKey("vel_lvl"))
		bool L_9;
		L_9 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, NULL);
		if (L_9)
		{
			goto IL_00c4;
		}
	}
	{
		// vel_level = 0;
		__this->___vel_level_15 = 0;
		// vel_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_10 = __this->___vel_txt_17;
		NullCheck(L_10);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_00d4;
	}

IL_00c4:
	{
		// vel_level = PlayerPrefs.GetInt("vel_lvl");
		int32_t L_11;
		L_11 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, NULL);
		__this->___vel_level_15 = L_11;
	}

IL_00d4:
	{
		// if (!PlayerPrefs.HasKey("scr_lvl"))
		bool L_12;
		L_12 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, NULL);
		if (L_12)
		{
			goto IL_00f9;
		}
	}
	{
		// scr_lvl = 0;
		__this->___scr_lvl_21 = 0;
		// scr_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_13 = __this->___scr_txt_23;
		NullCheck(L_13);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_0109;
	}

IL_00f9:
	{
		// scr_lvl = PlayerPrefs.GetInt("scr_lvl");
		int32_t L_14;
		L_14 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, NULL);
		__this->___scr_lvl_21 = L_14;
	}

IL_0109:
	{
		// if (!PlayerPrefs.HasKey("gra_lvl"))
		bool L_15;
		L_15 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, NULL);
		if (L_15)
		{
			goto IL_012e;
		}
	}
	{
		// gra_level = 0;
		__this->___gra_level_18 = 0;
		// gra_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_16 = __this->___gra_txt_20;
		NullCheck(L_16);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		goto IL_013e;
	}

IL_012e:
	{
		// gra_level = PlayerPrefs.GetInt("gra_lvl");
		int32_t L_17;
		L_17 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, NULL);
		__this->___gra_level_18 = L_17;
	}

IL_013e:
	{
		// bool homing = System.Convert.ToBoolean(PlayerPrefs.GetInt("homing"));
		int32_t L_18;
		L_18 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_19;
		L_19 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_18, NULL);
		V_0 = L_19;
		// if (!PlayerPrefs.HasKey("homing"))
		bool L_20;
		L_20 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1, NULL);
		if (L_20)
		{
			goto IL_015c;
		}
	}
	{
		// homing = false;
		V_0 = (bool)0;
	}

IL_015c:
	{
		// if (homing == false)
		bool L_21 = V_0;
		if (L_21)
		{
			goto IL_0171;
		}
	}
	{
		// homing_txt.text = "500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_22 = __this->___homing_txt_24;
		NullCheck(L_22);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_22, _stringLiteral50863FE64FB84E5522BDDE89CF1B95FBD9FEBDCA);
		goto IL_0181;
	}

IL_0171:
	{
		// homing_txt.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_23 = __this->___homing_txt_24;
		NullCheck(L_23);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
	}

IL_0181:
	{
		// bool split = System.Convert.ToBoolean(PlayerPrefs.GetInt("splitifier"));
		int32_t L_24;
		L_24 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_24, NULL);
		V_1 = L_25;
		// if (!PlayerPrefs.HasKey("splitifier"))
		bool L_26;
		L_26 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF, NULL);
		if (L_26)
		{
			goto IL_019f;
		}
	}
	{
		// split = false;
		V_1 = (bool)0;
	}

IL_019f:
	{
		// if (split == false)
		bool L_27 = V_1;
		if (L_27)
		{
			goto IL_01b4;
		}
	}
	{
		// splt_text.text = "1500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_28 = __this->___splt_text_25;
		NullCheck(L_28);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_28, _stringLiteralEA7A676433CAB71F816C191BA6205A79CA64BAF8);
		goto IL_01c4;
	}

IL_01b4:
	{
		// splt_text.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_29 = __this->___splt_text_25;
		NullCheck(L_29);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
	}

IL_01c4:
	{
		// bool crush = System.Convert.ToBoolean(PlayerPrefs.GetInt("crusher"));
		int32_t L_30;
		L_30 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral47BD0CAE8B2D419C4141C37C3DB237D0F602AA3E, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_31;
		L_31 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_30, NULL);
		V_2 = L_31;
		// if (!PlayerPrefs.HasKey("crusher"))
		bool L_32;
		L_32 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral47BD0CAE8B2D419C4141C37C3DB237D0F602AA3E, NULL);
		if (L_32)
		{
			goto IL_01e2;
		}
	}
	{
		// crush = false;
		V_2 = (bool)0;
	}

IL_01e2:
	{
		// if (crush == false)
		bool L_33 = V_2;
		if (L_33)
		{
			goto IL_01f7;
		}
	}
	{
		// spike_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_34 = __this->___spike_txt_26;
		NullCheck(L_34);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		goto IL_0207;
	}

IL_01f7:
	{
		// splt_text.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_35 = __this->___splt_text_25;
		NullCheck(L_35);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_35, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
	}

IL_0207:
	{
		// bool hyperbeam = System.Convert.ToBoolean(PlayerPrefs.GetInt("hyperbeam"));
		int32_t L_36;
		L_36 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_37;
		L_37 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_36, NULL);
		V_3 = L_37;
		// if (!PlayerPrefs.HasKey("hyperbeam"))
		bool L_38;
		L_38 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74, NULL);
		if (L_38)
		{
			goto IL_0225;
		}
	}
	{
		// hyperbeam= false;
		V_3 = (bool)0;
	}

IL_0225:
	{
		// if (hyperbeam == false)
		bool L_39 = V_3;
		if (L_39)
		{
			goto IL_023a;
		}
	}
	{
		// hyper_txt.text = "3000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_40 = __this->___hyper_txt_27;
		NullCheck(L_40);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_40, _stringLiteral70D790BE2680205977981BBCD96B3F0E742A057E);
		goto IL_024a;
	}

IL_023a:
	{
		// hyper_txt.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_41 = __this->___hyper_txt_27;
		NullCheck(L_41);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
	}

IL_024a:
	{
		// switch (level)
		int32_t L_42 = __this->___level_8;
		V_4 = L_42;
		int32_t L_43 = V_4;
		switch (L_43)
		{
			case 0:
			{
				goto IL_02ae;
			}
			case 1:
			{
				goto IL_0304;
			}
			case 2:
			{
				goto IL_035a;
			}
			case 3:
			{
				goto IL_03b0;
			}
			case 4:
			{
				goto IL_0406;
			}
			case 5:
			{
				goto IL_045c;
			}
			case 6:
			{
				goto IL_04b2;
			}
			case 7:
			{
				goto IL_0508;
			}
			case 8:
			{
				goto IL_055e;
			}
			case 9:
			{
				goto IL_05b4;
			}
			case 10:
			{
				goto IL_060a;
			}
			case 11:
			{
				goto IL_0660;
			}
			case 12:
			{
				goto IL_06b6;
			}
			case 13:
			{
				goto IL_070c;
			}
			case 14:
			{
				goto IL_0762;
			}
			case 15:
			{
				goto IL_07b8;
			}
			case 16:
			{
				goto IL_080e;
			}
			case 17:
			{
				goto IL_0864;
			}
			case 18:
			{
				goto IL_08ba;
			}
			case 19:
			{
				goto IL_090d;
			}
		}
	}
	{
		goto IL_095e;
	}

IL_02ae:
	{
		// for (int i = 0; i <= level; i++)
		V_5 = 0;
		goto IL_02e5;
	}

IL_02b3:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_mul_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_02e5:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___level_8;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_02b3;
		}
	}
	{
		// mul_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_53 = __this->___mul_txt_5;
		NullCheck(L_53);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_53, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		goto IL_095e;
	}

IL_0304:
	{
		// for (int i = 0; i <= level; i++)
		V_6 = 0;
		goto IL_033b;
	}

IL_0309:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_54 = __this->___lvl_mul_4;
		int32_t L_55 = V_6;
		NullCheck(L_54);
		int32_t L_56 = L_55;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_58;
		L_58 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_57, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_59;
		memset((&L_59), 0, sizeof(L_59));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_59), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_58);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_58, L_59);
		// for (int i = 0; i <= level; i++)
		int32_t L_60 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_60, 1));
	}

IL_033b:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_61 = V_6;
		int32_t L_62 = __this->___level_8;
		if ((((int32_t)L_61) <= ((int32_t)L_62)))
		{
			goto IL_0309;
		}
	}
	{
		// mul_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_63 = __this->___mul_txt_5;
		NullCheck(L_63);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_63, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		goto IL_095e;
	}

IL_035a:
	{
		// for (int i = 0; i <= level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_64 = __this->___lvl_mul_4;
		int32_t L_65 = V_7;
		NullCheck(L_64);
		int32_t L_66 = L_65;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		NullCheck(L_67);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_68;
		L_68 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_67, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_69;
		memset((&L_69), 0, sizeof(L_69));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_69), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_68, L_69);
		// for (int i = 0; i <= level; i++)
		int32_t L_70 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_70, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_71 = V_7;
		int32_t L_72 = __this->___level_8;
		if ((((int32_t)L_71) <= ((int32_t)L_72)))
		{
			goto IL_035f;
		}
	}
	{
		// mul_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_73 = __this->___mul_txt_5;
		NullCheck(L_73);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_73, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		goto IL_095e;
	}

IL_03b0:
	{
		// for (int i = 0; i <= level; i++)
		V_8 = 0;
		goto IL_03e7;
	}

IL_03b5:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_74 = __this->___lvl_mul_4;
		int32_t L_75 = V_8;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		NullCheck(L_77);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_78;
		L_78 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_77, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_79;
		memset((&L_79), 0, sizeof(L_79));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_79), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_78);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_78, L_79);
		// for (int i = 0; i <= level; i++)
		int32_t L_80 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_80, 1));
	}

IL_03e7:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_81 = V_8;
		int32_t L_82 = __this->___level_8;
		if ((((int32_t)L_81) <= ((int32_t)L_82)))
		{
			goto IL_03b5;
		}
	}
	{
		// mul_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_83 = __this->___mul_txt_5;
		NullCheck(L_83);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_83, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		goto IL_095e;
	}

IL_0406:
	{
		// for (int i = 0; i <= level; i++)
		V_9 = 0;
		goto IL_043d;
	}

IL_040b:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_84 = __this->___lvl_mul_4;
		int32_t L_85 = V_9;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		NullCheck(L_87);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_88;
		L_88 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_87, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_89;
		memset((&L_89), 0, sizeof(L_89));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_89), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_88);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_88, L_89);
		// for (int i = 0; i <= level; i++)
		int32_t L_90 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_90, 1));
	}

IL_043d:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_91 = V_9;
		int32_t L_92 = __this->___level_8;
		if ((((int32_t)L_91) <= ((int32_t)L_92)))
		{
			goto IL_040b;
		}
	}
	{
		// mul_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___mul_txt_5;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		goto IL_095e;
	}

IL_045c:
	{
		// for (int i = 0; i <= level; i++)
		V_10 = 0;
		goto IL_0493;
	}

IL_0461:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_94 = __this->___lvl_mul_4;
		int32_t L_95 = V_10;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		NullCheck(L_97);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_98;
		L_98 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_97, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_99;
		memset((&L_99), 0, sizeof(L_99));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_99), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_98);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_98, L_99);
		// for (int i = 0; i <= level; i++)
		int32_t L_100 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_100, 1));
	}

IL_0493:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_101 = V_10;
		int32_t L_102 = __this->___level_8;
		if ((((int32_t)L_101) <= ((int32_t)L_102)))
		{
			goto IL_0461;
		}
	}
	{
		// mul_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_103 = __this->___mul_txt_5;
		NullCheck(L_103);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_103, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		goto IL_095e;
	}

IL_04b2:
	{
		// for (int i = 0; i <= level; i++)
		V_11 = 0;
		goto IL_04e9;
	}

IL_04b7:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_104 = __this->___lvl_mul_4;
		int32_t L_105 = V_11;
		NullCheck(L_104);
		int32_t L_106 = L_105;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_107 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		NullCheck(L_107);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_108;
		L_108 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_107, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_109;
		memset((&L_109), 0, sizeof(L_109));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_109), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_108);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_108, L_109);
		// for (int i = 0; i <= level; i++)
		int32_t L_110 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_110, 1));
	}

IL_04e9:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_111 = V_11;
		int32_t L_112 = __this->___level_8;
		if ((((int32_t)L_111) <= ((int32_t)L_112)))
		{
			goto IL_04b7;
		}
	}
	{
		// mul_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_113 = __this->___mul_txt_5;
		NullCheck(L_113);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_113, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		goto IL_095e;
	}

IL_0508:
	{
		// for (int i = 0; i <= level; i++)
		V_12 = 0;
		goto IL_053f;
	}

IL_050d:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_114 = __this->___lvl_mul_4;
		int32_t L_115 = V_12;
		NullCheck(L_114);
		int32_t L_116 = L_115;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_117 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116));
		NullCheck(L_117);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_118;
		L_118 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_117, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_119;
		memset((&L_119), 0, sizeof(L_119));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_119), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_118);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_118, L_119);
		// for (int i = 0; i <= level; i++)
		int32_t L_120 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_120, 1));
	}

IL_053f:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_121 = V_12;
		int32_t L_122 = __this->___level_8;
		if ((((int32_t)L_121) <= ((int32_t)L_122)))
		{
			goto IL_050d;
		}
	}
	{
		// mul_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_123 = __this->___mul_txt_5;
		NullCheck(L_123);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_123, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		goto IL_095e;
	}

IL_055e:
	{
		// for (int i = 0; i <= level; i++)
		V_13 = 0;
		goto IL_0595;
	}

IL_0563:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_124 = __this->___lvl_mul_4;
		int32_t L_125 = V_13;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		NullCheck(L_127);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_128;
		L_128 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_127, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_129;
		memset((&L_129), 0, sizeof(L_129));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_129), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_128);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_128, L_129);
		// for (int i = 0; i <= level; i++)
		int32_t L_130 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_130, 1));
	}

IL_0595:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_131 = V_13;
		int32_t L_132 = __this->___level_8;
		if ((((int32_t)L_131) <= ((int32_t)L_132)))
		{
			goto IL_0563;
		}
	}
	{
		// mul_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_133 = __this->___mul_txt_5;
		NullCheck(L_133);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_133, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		goto IL_095e;
	}

IL_05b4:
	{
		// for (int i = 0; i <= level; i++)
		V_14 = 0;
		goto IL_05eb;
	}

IL_05b9:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_mul_4;
		int32_t L_135 = V_14;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= level; i++)
		int32_t L_140 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_05eb:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_141 = V_14;
		int32_t L_142 = __this->___level_8;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_05b9;
		}
	}
	{
		// mul_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_143 = __this->___mul_txt_5;
		NullCheck(L_143);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_143, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		goto IL_095e;
	}

IL_060a:
	{
		// for (int i = 0; i <= level; i++)
		V_15 = 0;
		goto IL_0641;
	}

IL_060f:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_144 = __this->___lvl_mul_4;
		int32_t L_145 = V_15;
		NullCheck(L_144);
		int32_t L_146 = L_145;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_147 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_146));
		NullCheck(L_147);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_148;
		L_148 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_147, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_149;
		memset((&L_149), 0, sizeof(L_149));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_149), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_148);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_148, L_149);
		// for (int i = 0; i <= level; i++)
		int32_t L_150 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_150, 1));
	}

IL_0641:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_151 = V_15;
		int32_t L_152 = __this->___level_8;
		if ((((int32_t)L_151) <= ((int32_t)L_152)))
		{
			goto IL_060f;
		}
	}
	{
		// mul_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_153 = __this->___mul_txt_5;
		NullCheck(L_153);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_153, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		goto IL_095e;
	}

IL_0660:
	{
		// for (int i = 0; i <= level; i++)
		V_16 = 0;
		goto IL_0697;
	}

IL_0665:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_154 = __this->___lvl_mul_4;
		int32_t L_155 = V_16;
		NullCheck(L_154);
		int32_t L_156 = L_155;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_157 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		NullCheck(L_157);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_158;
		L_158 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_157, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_159;
		memset((&L_159), 0, sizeof(L_159));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_159), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_158);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_158, L_159);
		// for (int i = 0; i <= level; i++)
		int32_t L_160 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_160, 1));
	}

IL_0697:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_161 = V_16;
		int32_t L_162 = __this->___level_8;
		if ((((int32_t)L_161) <= ((int32_t)L_162)))
		{
			goto IL_0665;
		}
	}
	{
		// mul_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_163 = __this->___mul_txt_5;
		NullCheck(L_163);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_163, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		goto IL_095e;
	}

IL_06b6:
	{
		// for (int i = 0; i <= level; i++)
		V_17 = 0;
		goto IL_06ed;
	}

IL_06bb:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_164 = __this->___lvl_mul_4;
		int32_t L_165 = V_17;
		NullCheck(L_164);
		int32_t L_166 = L_165;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_167 = (L_164)->GetAt(static_cast<il2cpp_array_size_t>(L_166));
		NullCheck(L_167);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_168;
		L_168 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_167, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_169;
		memset((&L_169), 0, sizeof(L_169));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_169), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_168);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_168, L_169);
		// for (int i = 0; i <= level; i++)
		int32_t L_170 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_170, 1));
	}

IL_06ed:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_171 = V_17;
		int32_t L_172 = __this->___level_8;
		if ((((int32_t)L_171) <= ((int32_t)L_172)))
		{
			goto IL_06bb;
		}
	}
	{
		// mul_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_173 = __this->___mul_txt_5;
		NullCheck(L_173);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_173, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		goto IL_095e;
	}

IL_070c:
	{
		// for (int i = 0; i <= level; i++)
		V_18 = 0;
		goto IL_0743;
	}

IL_0711:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_174 = __this->___lvl_mul_4;
		int32_t L_175 = V_18;
		NullCheck(L_174);
		int32_t L_176 = L_175;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_177 = (L_174)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		NullCheck(L_177);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_178;
		L_178 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_177, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_179;
		memset((&L_179), 0, sizeof(L_179));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_179), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_178);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_178, L_179);
		// for (int i = 0; i <= level; i++)
		int32_t L_180 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_180, 1));
	}

IL_0743:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_181 = V_18;
		int32_t L_182 = __this->___level_8;
		if ((((int32_t)L_181) <= ((int32_t)L_182)))
		{
			goto IL_0711;
		}
	}
	{
		// mul_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___mul_txt_5;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		goto IL_095e;
	}

IL_0762:
	{
		// for (int i = 0; i <= level; i++)
		V_19 = 0;
		goto IL_0799;
	}

IL_0767:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_184 = __this->___lvl_mul_4;
		int32_t L_185 = V_19;
		NullCheck(L_184);
		int32_t L_186 = L_185;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_187 = (L_184)->GetAt(static_cast<il2cpp_array_size_t>(L_186));
		NullCheck(L_187);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_188;
		L_188 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_187, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_189;
		memset((&L_189), 0, sizeof(L_189));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_189), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_188);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_188, L_189);
		// for (int i = 0; i <= level; i++)
		int32_t L_190 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_190, 1));
	}

IL_0799:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_191 = V_19;
		int32_t L_192 = __this->___level_8;
		if ((((int32_t)L_191) <= ((int32_t)L_192)))
		{
			goto IL_0767;
		}
	}
	{
		// mul_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_193 = __this->___mul_txt_5;
		NullCheck(L_193);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_193, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		goto IL_095e;
	}

IL_07b8:
	{
		// for (int i = 0; i <= level; i++)
		V_20 = 0;
		goto IL_07ef;
	}

IL_07bd:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_194 = __this->___lvl_mul_4;
		int32_t L_195 = V_20;
		NullCheck(L_194);
		int32_t L_196 = L_195;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_197 = (L_194)->GetAt(static_cast<il2cpp_array_size_t>(L_196));
		NullCheck(L_197);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_198;
		L_198 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_197, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_199;
		memset((&L_199), 0, sizeof(L_199));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_199), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_198);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_198, L_199);
		// for (int i = 0; i <= level; i++)
		int32_t L_200 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_200, 1));
	}

IL_07ef:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_201 = V_20;
		int32_t L_202 = __this->___level_8;
		if ((((int32_t)L_201) <= ((int32_t)L_202)))
		{
			goto IL_07bd;
		}
	}
	{
		// mul_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_203 = __this->___mul_txt_5;
		NullCheck(L_203);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_203, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		goto IL_095e;
	}

IL_080e:
	{
		// for (int i = 0; i <= level; i++)
		V_21 = 0;
		goto IL_0845;
	}

IL_0813:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_204 = __this->___lvl_mul_4;
		int32_t L_205 = V_21;
		NullCheck(L_204);
		int32_t L_206 = L_205;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_207 = (L_204)->GetAt(static_cast<il2cpp_array_size_t>(L_206));
		NullCheck(L_207);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_208;
		L_208 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_207, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_209;
		memset((&L_209), 0, sizeof(L_209));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_209), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_208);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_208, L_209);
		// for (int i = 0; i <= level; i++)
		int32_t L_210 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_210, 1));
	}

IL_0845:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_211 = V_21;
		int32_t L_212 = __this->___level_8;
		if ((((int32_t)L_211) <= ((int32_t)L_212)))
		{
			goto IL_0813;
		}
	}
	{
		// mul_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_213 = __this->___mul_txt_5;
		NullCheck(L_213);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_213, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		goto IL_095e;
	}

IL_0864:
	{
		// for (int i = 0; i <= level; i++)
		V_22 = 0;
		goto IL_089b;
	}

IL_0869:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_214 = __this->___lvl_mul_4;
		int32_t L_215 = V_22;
		NullCheck(L_214);
		int32_t L_216 = L_215;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_217 = (L_214)->GetAt(static_cast<il2cpp_array_size_t>(L_216));
		NullCheck(L_217);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_218;
		L_218 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_217, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_219;
		memset((&L_219), 0, sizeof(L_219));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_219), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_218);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_218, L_219);
		// for (int i = 0; i <= level; i++)
		int32_t L_220 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_220, 1));
	}

IL_089b:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_221 = V_22;
		int32_t L_222 = __this->___level_8;
		if ((((int32_t)L_221) <= ((int32_t)L_222)))
		{
			goto IL_0869;
		}
	}
	{
		// mul_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_223 = __this->___mul_txt_5;
		NullCheck(L_223);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_223, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		goto IL_095e;
	}

IL_08ba:
	{
		// for (int i = 0; i <= level; i++)
		V_23 = 0;
		goto IL_08f1;
	}

IL_08bf:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_mul_4;
		int32_t L_225 = V_23;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= level; i++)
		int32_t L_230 = V_23;
		V_23 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_08f1:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_231 = V_23;
		int32_t L_232 = __this->___level_8;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08bf;
		}
	}
	{
		// mul_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_233 = __this->___mul_txt_5;
		NullCheck(L_233);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_233, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		goto IL_095e;
	}

IL_090d:
	{
		// for (int i = 0; i <= level; i++)
		V_24 = 0;
		goto IL_0944;
	}

IL_0912:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_234 = __this->___lvl_mul_4;
		int32_t L_235 = V_24;
		NullCheck(L_234);
		int32_t L_236 = L_235;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_237 = (L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_236));
		NullCheck(L_237);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_238;
		L_238 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_237, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_239;
		memset((&L_239), 0, sizeof(L_239));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_239), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_238);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_238, L_239);
		// for (int i = 0; i <= level; i++)
		int32_t L_240 = V_24;
		V_24 = ((int32_t)il2cpp_codegen_add(L_240, 1));
	}

IL_0944:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_241 = V_24;
		int32_t L_242 = __this->___level_8;
		if ((((int32_t)L_241) <= ((int32_t)L_242)))
		{
			goto IL_0912;
		}
	}
	{
		// mul_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_243 = __this->___mul_txt_5;
		NullCheck(L_243);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_243, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
	}

IL_095e:
	{
		// switch (vel_level)
		int32_t L_244 = __this->___vel_level_15;
		V_4 = L_244;
		int32_t L_245 = V_4;
		switch (L_245)
		{
			case 0:
			{
				goto IL_09c2;
			}
			case 1:
			{
				goto IL_0a18;
			}
			case 2:
			{
				goto IL_0a6e;
			}
			case 3:
			{
				goto IL_0ac4;
			}
			case 4:
			{
				goto IL_0b1a;
			}
			case 5:
			{
				goto IL_0b70;
			}
			case 6:
			{
				goto IL_0bc6;
			}
			case 7:
			{
				goto IL_0c1c;
			}
			case 8:
			{
				goto IL_0c72;
			}
			case 9:
			{
				goto IL_0cc8;
			}
			case 10:
			{
				goto IL_0d1e;
			}
			case 11:
			{
				goto IL_0d74;
			}
			case 12:
			{
				goto IL_0dca;
			}
			case 13:
			{
				goto IL_0e20;
			}
			case 14:
			{
				goto IL_0e76;
			}
			case 15:
			{
				goto IL_0ecc;
			}
			case 16:
			{
				goto IL_0f22;
			}
			case 17:
			{
				goto IL_0f78;
			}
			case 18:
			{
				goto IL_0fce;
			}
			case 19:
			{
				goto IL_1021;
			}
		}
	}
	{
		goto IL_1072;
	}

IL_09c2:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_25 = 0;
		goto IL_09f9;
	}

IL_09c7:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_246 = __this->___lvl_vel_16;
		int32_t L_247 = V_25;
		NullCheck(L_246);
		int32_t L_248 = L_247;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_249 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		NullCheck(L_249);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_250;
		L_250 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_249, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_251;
		memset((&L_251), 0, sizeof(L_251));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_251), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_250);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_250, L_251);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_252 = V_25;
		V_25 = ((int32_t)il2cpp_codegen_add(L_252, 1));
	}

IL_09f9:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_253 = V_25;
		int32_t L_254 = __this->___vel_level_15;
		if ((((int32_t)L_253) <= ((int32_t)L_254)))
		{
			goto IL_09c7;
		}
	}
	{
		// vel_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___vel_txt_17;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		goto IL_1072;
	}

IL_0a18:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_26 = 0;
		goto IL_0a4f;
	}

IL_0a1d:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_256 = __this->___lvl_vel_16;
		int32_t L_257 = V_26;
		NullCheck(L_256);
		int32_t L_258 = L_257;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_259 = (L_256)->GetAt(static_cast<il2cpp_array_size_t>(L_258));
		NullCheck(L_259);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_260;
		L_260 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_259, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_261;
		memset((&L_261), 0, sizeof(L_261));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_261), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_260);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_260, L_261);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_262 = V_26;
		V_26 = ((int32_t)il2cpp_codegen_add(L_262, 1));
	}

IL_0a4f:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_263 = V_26;
		int32_t L_264 = __this->___vel_level_15;
		if ((((int32_t)L_263) <= ((int32_t)L_264)))
		{
			goto IL_0a1d;
		}
	}
	{
		// vel_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_265 = __this->___vel_txt_17;
		NullCheck(L_265);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_265, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		goto IL_1072;
	}

IL_0a6e:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_27 = 0;
		goto IL_0aa5;
	}

IL_0a73:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_266 = __this->___lvl_vel_16;
		int32_t L_267 = V_27;
		NullCheck(L_266);
		int32_t L_268 = L_267;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_269 = (L_266)->GetAt(static_cast<il2cpp_array_size_t>(L_268));
		NullCheck(L_269);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_270;
		L_270 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_269, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_271;
		memset((&L_271), 0, sizeof(L_271));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_271), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_270);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_270, L_271);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_272 = V_27;
		V_27 = ((int32_t)il2cpp_codegen_add(L_272, 1));
	}

IL_0aa5:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_273 = V_27;
		int32_t L_274 = __this->___vel_level_15;
		if ((((int32_t)L_273) <= ((int32_t)L_274)))
		{
			goto IL_0a73;
		}
	}
	{
		// vel_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_275 = __this->___vel_txt_17;
		NullCheck(L_275);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_275, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		goto IL_1072;
	}

IL_0ac4:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_28 = 0;
		goto IL_0afb;
	}

IL_0ac9:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_276 = __this->___lvl_vel_16;
		int32_t L_277 = V_28;
		NullCheck(L_276);
		int32_t L_278 = L_277;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_279 = (L_276)->GetAt(static_cast<il2cpp_array_size_t>(L_278));
		NullCheck(L_279);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_280;
		L_280 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_279, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_281;
		memset((&L_281), 0, sizeof(L_281));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_281), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_280);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_280, L_281);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_282 = V_28;
		V_28 = ((int32_t)il2cpp_codegen_add(L_282, 1));
	}

IL_0afb:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_283 = V_28;
		int32_t L_284 = __this->___vel_level_15;
		if ((((int32_t)L_283) <= ((int32_t)L_284)))
		{
			goto IL_0ac9;
		}
	}
	{
		// vel_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_285 = __this->___vel_txt_17;
		NullCheck(L_285);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_285, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		goto IL_1072;
	}

IL_0b1a:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_29 = 0;
		goto IL_0b51;
	}

IL_0b1f:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_286 = __this->___lvl_vel_16;
		int32_t L_287 = V_29;
		NullCheck(L_286);
		int32_t L_288 = L_287;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_289 = (L_286)->GetAt(static_cast<il2cpp_array_size_t>(L_288));
		NullCheck(L_289);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_290;
		L_290 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_289, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_291;
		memset((&L_291), 0, sizeof(L_291));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_291), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_290);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_290, L_291);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_292 = V_29;
		V_29 = ((int32_t)il2cpp_codegen_add(L_292, 1));
	}

IL_0b51:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_293 = V_29;
		int32_t L_294 = __this->___vel_level_15;
		if ((((int32_t)L_293) <= ((int32_t)L_294)))
		{
			goto IL_0b1f;
		}
	}
	{
		// vel_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_295 = __this->___vel_txt_17;
		NullCheck(L_295);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_295, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		goto IL_1072;
	}

IL_0b70:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_30 = 0;
		goto IL_0ba7;
	}

IL_0b75:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_vel_16;
		int32_t L_297 = V_30;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_302 = V_30;
		V_30 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0ba7:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_303 = V_30;
		int32_t L_304 = __this->___vel_level_15;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b75;
		}
	}
	{
		// vel_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_305 = __this->___vel_txt_17;
		NullCheck(L_305);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_305, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		goto IL_1072;
	}

IL_0bc6:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_31 = 0;
		goto IL_0bfd;
	}

IL_0bcb:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_306 = __this->___lvl_vel_16;
		int32_t L_307 = V_31;
		NullCheck(L_306);
		int32_t L_308 = L_307;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_309 = (L_306)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		NullCheck(L_309);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_310;
		L_310 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_309, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_311;
		memset((&L_311), 0, sizeof(L_311));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_311), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_310);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_310, L_311);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_312 = V_31;
		V_31 = ((int32_t)il2cpp_codegen_add(L_312, 1));
	}

IL_0bfd:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_313 = V_31;
		int32_t L_314 = __this->___vel_level_15;
		if ((((int32_t)L_313) <= ((int32_t)L_314)))
		{
			goto IL_0bcb;
		}
	}
	{
		// vel_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_315 = __this->___vel_txt_17;
		NullCheck(L_315);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_315, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		goto IL_1072;
	}

IL_0c1c:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_32 = 0;
		goto IL_0c53;
	}

IL_0c21:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_316 = __this->___lvl_vel_16;
		int32_t L_317 = V_32;
		NullCheck(L_316);
		int32_t L_318 = L_317;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_319 = (L_316)->GetAt(static_cast<il2cpp_array_size_t>(L_318));
		NullCheck(L_319);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_320;
		L_320 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_319, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_321;
		memset((&L_321), 0, sizeof(L_321));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_321), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_320);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_320, L_321);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_322 = V_32;
		V_32 = ((int32_t)il2cpp_codegen_add(L_322, 1));
	}

IL_0c53:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_323 = V_32;
		int32_t L_324 = __this->___vel_level_15;
		if ((((int32_t)L_323) <= ((int32_t)L_324)))
		{
			goto IL_0c21;
		}
	}
	{
		// vel_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_325 = __this->___vel_txt_17;
		NullCheck(L_325);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_325, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		goto IL_1072;
	}

IL_0c72:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_33 = 0;
		goto IL_0ca9;
	}

IL_0c77:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_326 = __this->___lvl_vel_16;
		int32_t L_327 = V_33;
		NullCheck(L_326);
		int32_t L_328 = L_327;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_329 = (L_326)->GetAt(static_cast<il2cpp_array_size_t>(L_328));
		NullCheck(L_329);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_330;
		L_330 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_329, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_331;
		memset((&L_331), 0, sizeof(L_331));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_331), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_330);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_330, L_331);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_332 = V_33;
		V_33 = ((int32_t)il2cpp_codegen_add(L_332, 1));
	}

IL_0ca9:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_333 = V_33;
		int32_t L_334 = __this->___vel_level_15;
		if ((((int32_t)L_333) <= ((int32_t)L_334)))
		{
			goto IL_0c77;
		}
	}
	{
		// vel_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_335 = __this->___vel_txt_17;
		NullCheck(L_335);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_335, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		goto IL_1072;
	}

IL_0cc8:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_34 = 0;
		goto IL_0cff;
	}

IL_0ccd:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_336 = __this->___lvl_vel_16;
		int32_t L_337 = V_34;
		NullCheck(L_336);
		int32_t L_338 = L_337;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_339 = (L_336)->GetAt(static_cast<il2cpp_array_size_t>(L_338));
		NullCheck(L_339);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_340;
		L_340 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_339, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_341;
		memset((&L_341), 0, sizeof(L_341));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_341), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_340);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_340, L_341);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_342 = V_34;
		V_34 = ((int32_t)il2cpp_codegen_add(L_342, 1));
	}

IL_0cff:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_343 = V_34;
		int32_t L_344 = __this->___vel_level_15;
		if ((((int32_t)L_343) <= ((int32_t)L_344)))
		{
			goto IL_0ccd;
		}
	}
	{
		// vel_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___vel_txt_17;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		goto IL_1072;
	}

IL_0d1e:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_35 = 0;
		goto IL_0d55;
	}

IL_0d23:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_346 = __this->___lvl_vel_16;
		int32_t L_347 = V_35;
		NullCheck(L_346);
		int32_t L_348 = L_347;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_349 = (L_346)->GetAt(static_cast<il2cpp_array_size_t>(L_348));
		NullCheck(L_349);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_350;
		L_350 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_349, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_351;
		memset((&L_351), 0, sizeof(L_351));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_351), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_350);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_350, L_351);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_352 = V_35;
		V_35 = ((int32_t)il2cpp_codegen_add(L_352, 1));
	}

IL_0d55:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_353 = V_35;
		int32_t L_354 = __this->___vel_level_15;
		if ((((int32_t)L_353) <= ((int32_t)L_354)))
		{
			goto IL_0d23;
		}
	}
	{
		// vel_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_355 = __this->___vel_txt_17;
		NullCheck(L_355);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_355, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		goto IL_1072;
	}

IL_0d74:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_36 = 0;
		goto IL_0dab;
	}

IL_0d79:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_356 = __this->___lvl_vel_16;
		int32_t L_357 = V_36;
		NullCheck(L_356);
		int32_t L_358 = L_357;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_359 = (L_356)->GetAt(static_cast<il2cpp_array_size_t>(L_358));
		NullCheck(L_359);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_360;
		L_360 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_359, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_361;
		memset((&L_361), 0, sizeof(L_361));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_361), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_360);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_360, L_361);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_362 = V_36;
		V_36 = ((int32_t)il2cpp_codegen_add(L_362, 1));
	}

IL_0dab:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_363 = V_36;
		int32_t L_364 = __this->___vel_level_15;
		if ((((int32_t)L_363) <= ((int32_t)L_364)))
		{
			goto IL_0d79;
		}
	}
	{
		// vel_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_365 = __this->___vel_txt_17;
		NullCheck(L_365);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_365, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		goto IL_1072;
	}

IL_0dca:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_37 = 0;
		goto IL_0e01;
	}

IL_0dcf:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_366 = __this->___lvl_vel_16;
		int32_t L_367 = V_37;
		NullCheck(L_366);
		int32_t L_368 = L_367;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_369 = (L_366)->GetAt(static_cast<il2cpp_array_size_t>(L_368));
		NullCheck(L_369);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_370;
		L_370 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_369, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_371;
		memset((&L_371), 0, sizeof(L_371));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_371), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_370);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_370, L_371);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_372 = V_37;
		V_37 = ((int32_t)il2cpp_codegen_add(L_372, 1));
	}

IL_0e01:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_373 = V_37;
		int32_t L_374 = __this->___vel_level_15;
		if ((((int32_t)L_373) <= ((int32_t)L_374)))
		{
			goto IL_0dcf;
		}
	}
	{
		// vel_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_375 = __this->___vel_txt_17;
		NullCheck(L_375);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_375, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		goto IL_1072;
	}

IL_0e20:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_38 = 0;
		goto IL_0e57;
	}

IL_0e25:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_376 = __this->___lvl_vel_16;
		int32_t L_377 = V_38;
		NullCheck(L_376);
		int32_t L_378 = L_377;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_379 = (L_376)->GetAt(static_cast<il2cpp_array_size_t>(L_378));
		NullCheck(L_379);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_380;
		L_380 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_379, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_381;
		memset((&L_381), 0, sizeof(L_381));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_381), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_380);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_380, L_381);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_382 = V_38;
		V_38 = ((int32_t)il2cpp_codegen_add(L_382, 1));
	}

IL_0e57:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_383 = V_38;
		int32_t L_384 = __this->___vel_level_15;
		if ((((int32_t)L_383) <= ((int32_t)L_384)))
		{
			goto IL_0e25;
		}
	}
	{
		// vel_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_385 = __this->___vel_txt_17;
		NullCheck(L_385);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_385, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		goto IL_1072;
	}

IL_0e76:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_39 = 0;
		goto IL_0ead;
	}

IL_0e7b:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_386 = __this->___lvl_vel_16;
		int32_t L_387 = V_39;
		NullCheck(L_386);
		int32_t L_388 = L_387;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_389 = (L_386)->GetAt(static_cast<il2cpp_array_size_t>(L_388));
		NullCheck(L_389);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_390;
		L_390 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_389, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_391;
		memset((&L_391), 0, sizeof(L_391));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_391), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_390);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_390, L_391);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_392 = V_39;
		V_39 = ((int32_t)il2cpp_codegen_add(L_392, 1));
	}

IL_0ead:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_393 = V_39;
		int32_t L_394 = __this->___vel_level_15;
		if ((((int32_t)L_393) <= ((int32_t)L_394)))
		{
			goto IL_0e7b;
		}
	}
	{
		// vel_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_395 = __this->___vel_txt_17;
		NullCheck(L_395);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_395, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		goto IL_1072;
	}

IL_0ecc:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_40 = 0;
		goto IL_0f03;
	}

IL_0ed1:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_396 = __this->___lvl_vel_16;
		int32_t L_397 = V_40;
		NullCheck(L_396);
		int32_t L_398 = L_397;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_399 = (L_396)->GetAt(static_cast<il2cpp_array_size_t>(L_398));
		NullCheck(L_399);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_400;
		L_400 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_399, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_401;
		memset((&L_401), 0, sizeof(L_401));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_401), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_400);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_400, L_401);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_402 = V_40;
		V_40 = ((int32_t)il2cpp_codegen_add(L_402, 1));
	}

IL_0f03:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_403 = V_40;
		int32_t L_404 = __this->___vel_level_15;
		if ((((int32_t)L_403) <= ((int32_t)L_404)))
		{
			goto IL_0ed1;
		}
	}
	{
		// vel_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_405 = __this->___vel_txt_17;
		NullCheck(L_405);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_405, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		goto IL_1072;
	}

IL_0f22:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_41 = 0;
		goto IL_0f59;
	}

IL_0f27:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_406 = __this->___lvl_vel_16;
		int32_t L_407 = V_41;
		NullCheck(L_406);
		int32_t L_408 = L_407;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_409 = (L_406)->GetAt(static_cast<il2cpp_array_size_t>(L_408));
		NullCheck(L_409);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_410;
		L_410 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_409, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_411;
		memset((&L_411), 0, sizeof(L_411));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_411), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_410);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_410, L_411);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_412 = V_41;
		V_41 = ((int32_t)il2cpp_codegen_add(L_412, 1));
	}

IL_0f59:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_413 = V_41;
		int32_t L_414 = __this->___vel_level_15;
		if ((((int32_t)L_413) <= ((int32_t)L_414)))
		{
			goto IL_0f27;
		}
	}
	{
		// vel_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_415 = __this->___vel_txt_17;
		NullCheck(L_415);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_415, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		goto IL_1072;
	}

IL_0f78:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_42 = 0;
		goto IL_0faf;
	}

IL_0f7d:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_416 = __this->___lvl_vel_16;
		int32_t L_417 = V_42;
		NullCheck(L_416);
		int32_t L_418 = L_417;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_419 = (L_416)->GetAt(static_cast<il2cpp_array_size_t>(L_418));
		NullCheck(L_419);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_420;
		L_420 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_419, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_421;
		memset((&L_421), 0, sizeof(L_421));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_421), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_420);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_420, L_421);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_422 = V_42;
		V_42 = ((int32_t)il2cpp_codegen_add(L_422, 1));
	}

IL_0faf:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_423 = V_42;
		int32_t L_424 = __this->___vel_level_15;
		if ((((int32_t)L_423) <= ((int32_t)L_424)))
		{
			goto IL_0f7d;
		}
	}
	{
		// vel_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_425 = __this->___vel_txt_17;
		NullCheck(L_425);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_425, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		goto IL_1072;
	}

IL_0fce:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_43 = 0;
		goto IL_1005;
	}

IL_0fd3:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_426 = __this->___lvl_vel_16;
		int32_t L_427 = V_43;
		NullCheck(L_426);
		int32_t L_428 = L_427;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_429 = (L_426)->GetAt(static_cast<il2cpp_array_size_t>(L_428));
		NullCheck(L_429);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_430;
		L_430 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_429, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_431;
		memset((&L_431), 0, sizeof(L_431));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_431), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_430);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_430, L_431);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_432 = V_43;
		V_43 = ((int32_t)il2cpp_codegen_add(L_432, 1));
	}

IL_1005:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_433 = V_43;
		int32_t L_434 = __this->___vel_level_15;
		if ((((int32_t)L_433) <= ((int32_t)L_434)))
		{
			goto IL_0fd3;
		}
	}
	{
		// vel_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_435 = __this->___vel_txt_17;
		NullCheck(L_435);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_435, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		goto IL_1072;
	}

IL_1021:
	{
		// for (int i = 0; i <= vel_level; i++)
		V_44 = 0;
		goto IL_1058;
	}

IL_1026:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_436 = __this->___lvl_vel_16;
		int32_t L_437 = V_44;
		NullCheck(L_436);
		int32_t L_438 = L_437;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_439 = (L_436)->GetAt(static_cast<il2cpp_array_size_t>(L_438));
		NullCheck(L_439);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_440;
		L_440 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_439, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_441;
		memset((&L_441), 0, sizeof(L_441));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_441), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_440);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_440, L_441);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_442 = V_44;
		V_44 = ((int32_t)il2cpp_codegen_add(L_442, 1));
	}

IL_1058:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_443 = V_44;
		int32_t L_444 = __this->___vel_level_15;
		if ((((int32_t)L_443) <= ((int32_t)L_444)))
		{
			goto IL_1026;
		}
	}
	{
		// vel_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_445 = __this->___vel_txt_17;
		NullCheck(L_445);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_445, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
	}

IL_1072:
	{
		// switch (coin_level)
		int32_t L_446 = __this->___coin_level_12;
		V_4 = L_446;
		int32_t L_447 = V_4;
		switch (L_447)
		{
			case 0:
			{
				goto IL_10d6;
			}
			case 1:
			{
				goto IL_112c;
			}
			case 2:
			{
				goto IL_1182;
			}
			case 3:
			{
				goto IL_11d8;
			}
			case 4:
			{
				goto IL_122e;
			}
			case 5:
			{
				goto IL_1284;
			}
			case 6:
			{
				goto IL_12da;
			}
			case 7:
			{
				goto IL_1330;
			}
			case 8:
			{
				goto IL_1386;
			}
			case 9:
			{
				goto IL_13dc;
			}
			case 10:
			{
				goto IL_1432;
			}
			case 11:
			{
				goto IL_1488;
			}
			case 12:
			{
				goto IL_14de;
			}
			case 13:
			{
				goto IL_1534;
			}
			case 14:
			{
				goto IL_158a;
			}
			case 15:
			{
				goto IL_15e0;
			}
			case 16:
			{
				goto IL_1636;
			}
			case 17:
			{
				goto IL_168c;
			}
			case 18:
			{
				goto IL_16e2;
			}
			case 19:
			{
				goto IL_1735;
			}
		}
	}
	{
		goto IL_1786;
	}

IL_10d6:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_45 = 0;
		goto IL_110d;
	}

IL_10db:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_448 = __this->___lvl_coin_13;
		int32_t L_449 = V_45;
		NullCheck(L_448);
		int32_t L_450 = L_449;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_451 = (L_448)->GetAt(static_cast<il2cpp_array_size_t>(L_450));
		NullCheck(L_451);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_452;
		L_452 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_451, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_453;
		memset((&L_453), 0, sizeof(L_453));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_453), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_452);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_452, L_453);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_454 = V_45;
		V_45 = ((int32_t)il2cpp_codegen_add(L_454, 1));
	}

IL_110d:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_455 = V_45;
		int32_t L_456 = __this->___coin_level_12;
		if ((((int32_t)L_455) <= ((int32_t)L_456)))
		{
			goto IL_10db;
		}
	}
	{
		// coin_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_457 = __this->___coin_txt_14;
		NullCheck(L_457);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_457, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		goto IL_1786;
	}

IL_112c:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_46 = 0;
		goto IL_1163;
	}

IL_1131:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_458 = __this->___lvl_coin_13;
		int32_t L_459 = V_46;
		NullCheck(L_458);
		int32_t L_460 = L_459;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_461 = (L_458)->GetAt(static_cast<il2cpp_array_size_t>(L_460));
		NullCheck(L_461);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_462;
		L_462 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_461, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_463;
		memset((&L_463), 0, sizeof(L_463));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_463), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_462);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_462, L_463);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_464 = V_46;
		V_46 = ((int32_t)il2cpp_codegen_add(L_464, 1));
	}

IL_1163:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_465 = V_46;
		int32_t L_466 = __this->___coin_level_12;
		if ((((int32_t)L_465) <= ((int32_t)L_466)))
		{
			goto IL_1131;
		}
	}
	{
		// coin_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_467 = __this->___coin_txt_14;
		NullCheck(L_467);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_467, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		goto IL_1786;
	}

IL_1182:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_47 = 0;
		goto IL_11b9;
	}

IL_1187:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_468 = __this->___lvl_coin_13;
		int32_t L_469 = V_47;
		NullCheck(L_468);
		int32_t L_470 = L_469;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_471 = (L_468)->GetAt(static_cast<il2cpp_array_size_t>(L_470));
		NullCheck(L_471);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_472;
		L_472 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_471, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_473;
		memset((&L_473), 0, sizeof(L_473));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_473), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_472);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_472, L_473);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_474 = V_47;
		V_47 = ((int32_t)il2cpp_codegen_add(L_474, 1));
	}

IL_11b9:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_475 = V_47;
		int32_t L_476 = __this->___coin_level_12;
		if ((((int32_t)L_475) <= ((int32_t)L_476)))
		{
			goto IL_1187;
		}
	}
	{
		// coin_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_477 = __this->___coin_txt_14;
		NullCheck(L_477);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_477, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		goto IL_1786;
	}

IL_11d8:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_48 = 0;
		goto IL_120f;
	}

IL_11dd:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_478 = __this->___lvl_coin_13;
		int32_t L_479 = V_48;
		NullCheck(L_478);
		int32_t L_480 = L_479;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_481 = (L_478)->GetAt(static_cast<il2cpp_array_size_t>(L_480));
		NullCheck(L_481);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_482;
		L_482 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_481, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_483;
		memset((&L_483), 0, sizeof(L_483));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_483), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_482);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_482, L_483);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_484 = V_48;
		V_48 = ((int32_t)il2cpp_codegen_add(L_484, 1));
	}

IL_120f:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_485 = V_48;
		int32_t L_486 = __this->___coin_level_12;
		if ((((int32_t)L_485) <= ((int32_t)L_486)))
		{
			goto IL_11dd;
		}
	}
	{
		// coin_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_487 = __this->___coin_txt_14;
		NullCheck(L_487);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_487, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		goto IL_1786;
	}

IL_122e:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_49 = 0;
		goto IL_1265;
	}

IL_1233:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_488 = __this->___lvl_coin_13;
		int32_t L_489 = V_49;
		NullCheck(L_488);
		int32_t L_490 = L_489;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_491 = (L_488)->GetAt(static_cast<il2cpp_array_size_t>(L_490));
		NullCheck(L_491);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_492;
		L_492 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_491, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_493;
		memset((&L_493), 0, sizeof(L_493));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_493), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_492);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_492, L_493);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_494 = V_49;
		V_49 = ((int32_t)il2cpp_codegen_add(L_494, 1));
	}

IL_1265:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_495 = V_49;
		int32_t L_496 = __this->___coin_level_12;
		if ((((int32_t)L_495) <= ((int32_t)L_496)))
		{
			goto IL_1233;
		}
	}
	{
		// coin_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_497 = __this->___coin_txt_14;
		NullCheck(L_497);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_497, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		goto IL_1786;
	}

IL_1284:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_50 = 0;
		goto IL_12bb;
	}

IL_1289:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_498 = __this->___lvl_coin_13;
		int32_t L_499 = V_50;
		NullCheck(L_498);
		int32_t L_500 = L_499;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_501 = (L_498)->GetAt(static_cast<il2cpp_array_size_t>(L_500));
		NullCheck(L_501);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_502;
		L_502 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_501, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_503;
		memset((&L_503), 0, sizeof(L_503));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_503), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_502);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_502, L_503);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_504 = V_50;
		V_50 = ((int32_t)il2cpp_codegen_add(L_504, 1));
	}

IL_12bb:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_505 = V_50;
		int32_t L_506 = __this->___coin_level_12;
		if ((((int32_t)L_505) <= ((int32_t)L_506)))
		{
			goto IL_1289;
		}
	}
	{
		// coin_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_507 = __this->___coin_txt_14;
		NullCheck(L_507);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_507, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		goto IL_1786;
	}

IL_12da:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_51 = 0;
		goto IL_1311;
	}

IL_12df:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_508 = __this->___lvl_coin_13;
		int32_t L_509 = V_51;
		NullCheck(L_508);
		int32_t L_510 = L_509;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_511 = (L_508)->GetAt(static_cast<il2cpp_array_size_t>(L_510));
		NullCheck(L_511);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_512;
		L_512 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_511, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_513;
		memset((&L_513), 0, sizeof(L_513));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_513), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_512);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_512, L_513);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_514 = V_51;
		V_51 = ((int32_t)il2cpp_codegen_add(L_514, 1));
	}

IL_1311:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_515 = V_51;
		int32_t L_516 = __this->___coin_level_12;
		if ((((int32_t)L_515) <= ((int32_t)L_516)))
		{
			goto IL_12df;
		}
	}
	{
		// coin_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_517 = __this->___coin_txt_14;
		NullCheck(L_517);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_517, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		goto IL_1786;
	}

IL_1330:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_52 = 0;
		goto IL_1367;
	}

IL_1335:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_518 = __this->___lvl_coin_13;
		int32_t L_519 = V_52;
		NullCheck(L_518);
		int32_t L_520 = L_519;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_521 = (L_518)->GetAt(static_cast<il2cpp_array_size_t>(L_520));
		NullCheck(L_521);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_522;
		L_522 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_521, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_523;
		memset((&L_523), 0, sizeof(L_523));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_523), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_522);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_522, L_523);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_524 = V_52;
		V_52 = ((int32_t)il2cpp_codegen_add(L_524, 1));
	}

IL_1367:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_525 = V_52;
		int32_t L_526 = __this->___coin_level_12;
		if ((((int32_t)L_525) <= ((int32_t)L_526)))
		{
			goto IL_1335;
		}
	}
	{
		// coin_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_527 = __this->___coin_txt_14;
		NullCheck(L_527);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_527, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		goto IL_1786;
	}

IL_1386:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_53 = 0;
		goto IL_13bd;
	}

IL_138b:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_528 = __this->___lvl_coin_13;
		int32_t L_529 = V_53;
		NullCheck(L_528);
		int32_t L_530 = L_529;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_531 = (L_528)->GetAt(static_cast<il2cpp_array_size_t>(L_530));
		NullCheck(L_531);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_532;
		L_532 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_531, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_533;
		memset((&L_533), 0, sizeof(L_533));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_533), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_532);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_532, L_533);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_534 = V_53;
		V_53 = ((int32_t)il2cpp_codegen_add(L_534, 1));
	}

IL_13bd:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_535 = V_53;
		int32_t L_536 = __this->___coin_level_12;
		if ((((int32_t)L_535) <= ((int32_t)L_536)))
		{
			goto IL_138b;
		}
	}
	{
		// coin_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_537 = __this->___coin_txt_14;
		NullCheck(L_537);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_537, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		goto IL_1786;
	}

IL_13dc:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_54 = 0;
		goto IL_1413;
	}

IL_13e1:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_538 = __this->___lvl_coin_13;
		int32_t L_539 = V_54;
		NullCheck(L_538);
		int32_t L_540 = L_539;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_541 = (L_538)->GetAt(static_cast<il2cpp_array_size_t>(L_540));
		NullCheck(L_541);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_542;
		L_542 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_541, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_543;
		memset((&L_543), 0, sizeof(L_543));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_543), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_542);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_542, L_543);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_544 = V_54;
		V_54 = ((int32_t)il2cpp_codegen_add(L_544, 1));
	}

IL_1413:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_545 = V_54;
		int32_t L_546 = __this->___coin_level_12;
		if ((((int32_t)L_545) <= ((int32_t)L_546)))
		{
			goto IL_13e1;
		}
	}
	{
		// coin_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_547 = __this->___coin_txt_14;
		NullCheck(L_547);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_547, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		goto IL_1786;
	}

IL_1432:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_55 = 0;
		goto IL_1469;
	}

IL_1437:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_548 = __this->___lvl_coin_13;
		int32_t L_549 = V_55;
		NullCheck(L_548);
		int32_t L_550 = L_549;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_551 = (L_548)->GetAt(static_cast<il2cpp_array_size_t>(L_550));
		NullCheck(L_551);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_552;
		L_552 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_551, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_553;
		memset((&L_553), 0, sizeof(L_553));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_553), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_552);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_552, L_553);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_554 = V_55;
		V_55 = ((int32_t)il2cpp_codegen_add(L_554, 1));
	}

IL_1469:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_555 = V_55;
		int32_t L_556 = __this->___coin_level_12;
		if ((((int32_t)L_555) <= ((int32_t)L_556)))
		{
			goto IL_1437;
		}
	}
	{
		// coin_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_557 = __this->___coin_txt_14;
		NullCheck(L_557);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_557, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		goto IL_1786;
	}

IL_1488:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_56 = 0;
		goto IL_14bf;
	}

IL_148d:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_558 = __this->___lvl_coin_13;
		int32_t L_559 = V_56;
		NullCheck(L_558);
		int32_t L_560 = L_559;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_561 = (L_558)->GetAt(static_cast<il2cpp_array_size_t>(L_560));
		NullCheck(L_561);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_562;
		L_562 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_561, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_563;
		memset((&L_563), 0, sizeof(L_563));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_563), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_562);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_562, L_563);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_564 = V_56;
		V_56 = ((int32_t)il2cpp_codegen_add(L_564, 1));
	}

IL_14bf:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_565 = V_56;
		int32_t L_566 = __this->___coin_level_12;
		if ((((int32_t)L_565) <= ((int32_t)L_566)))
		{
			goto IL_148d;
		}
	}
	{
		// coin_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_567 = __this->___coin_txt_14;
		NullCheck(L_567);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_567, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		goto IL_1786;
	}

IL_14de:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_57 = 0;
		goto IL_1515;
	}

IL_14e3:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_568 = __this->___lvl_coin_13;
		int32_t L_569 = V_57;
		NullCheck(L_568);
		int32_t L_570 = L_569;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_571 = (L_568)->GetAt(static_cast<il2cpp_array_size_t>(L_570));
		NullCheck(L_571);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_572;
		L_572 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_571, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_573;
		memset((&L_573), 0, sizeof(L_573));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_573), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_572);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_572, L_573);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_574 = V_57;
		V_57 = ((int32_t)il2cpp_codegen_add(L_574, 1));
	}

IL_1515:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_575 = V_57;
		int32_t L_576 = __this->___coin_level_12;
		if ((((int32_t)L_575) <= ((int32_t)L_576)))
		{
			goto IL_14e3;
		}
	}
	{
		// coin_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_577 = __this->___coin_txt_14;
		NullCheck(L_577);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_577, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		goto IL_1786;
	}

IL_1534:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_58 = 0;
		goto IL_156b;
	}

IL_1539:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_578 = __this->___lvl_coin_13;
		int32_t L_579 = V_58;
		NullCheck(L_578);
		int32_t L_580 = L_579;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_581 = (L_578)->GetAt(static_cast<il2cpp_array_size_t>(L_580));
		NullCheck(L_581);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_582;
		L_582 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_581, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_583;
		memset((&L_583), 0, sizeof(L_583));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_583), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_582);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_582, L_583);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_584 = V_58;
		V_58 = ((int32_t)il2cpp_codegen_add(L_584, 1));
	}

IL_156b:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_585 = V_58;
		int32_t L_586 = __this->___coin_level_12;
		if ((((int32_t)L_585) <= ((int32_t)L_586)))
		{
			goto IL_1539;
		}
	}
	{
		// coin_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_587 = __this->___coin_txt_14;
		NullCheck(L_587);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_587, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		goto IL_1786;
	}

IL_158a:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_59 = 0;
		goto IL_15c1;
	}

IL_158f:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_588 = __this->___lvl_coin_13;
		int32_t L_589 = V_59;
		NullCheck(L_588);
		int32_t L_590 = L_589;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_591 = (L_588)->GetAt(static_cast<il2cpp_array_size_t>(L_590));
		NullCheck(L_591);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_592;
		L_592 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_591, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_593;
		memset((&L_593), 0, sizeof(L_593));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_593), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_592);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_592, L_593);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_594 = V_59;
		V_59 = ((int32_t)il2cpp_codegen_add(L_594, 1));
	}

IL_15c1:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_595 = V_59;
		int32_t L_596 = __this->___coin_level_12;
		if ((((int32_t)L_595) <= ((int32_t)L_596)))
		{
			goto IL_158f;
		}
	}
	{
		// mul_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_597 = __this->___mul_txt_5;
		NullCheck(L_597);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_597, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		goto IL_1786;
	}

IL_15e0:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_60 = 0;
		goto IL_1617;
	}

IL_15e5:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_598 = __this->___lvl_coin_13;
		int32_t L_599 = V_60;
		NullCheck(L_598);
		int32_t L_600 = L_599;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_601 = (L_598)->GetAt(static_cast<il2cpp_array_size_t>(L_600));
		NullCheck(L_601);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_602;
		L_602 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_601, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_603;
		memset((&L_603), 0, sizeof(L_603));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_603), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_602);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_602, L_603);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_604 = V_60;
		V_60 = ((int32_t)il2cpp_codegen_add(L_604, 1));
	}

IL_1617:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_605 = V_60;
		int32_t L_606 = __this->___coin_level_12;
		if ((((int32_t)L_605) <= ((int32_t)L_606)))
		{
			goto IL_15e5;
		}
	}
	{
		// coin_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_607 = __this->___coin_txt_14;
		NullCheck(L_607);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_607, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		goto IL_1786;
	}

IL_1636:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_61 = 0;
		goto IL_166d;
	}

IL_163b:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_608 = __this->___lvl_coin_13;
		int32_t L_609 = V_61;
		NullCheck(L_608);
		int32_t L_610 = L_609;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_611 = (L_608)->GetAt(static_cast<il2cpp_array_size_t>(L_610));
		NullCheck(L_611);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_612;
		L_612 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_611, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_613;
		memset((&L_613), 0, sizeof(L_613));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_613), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_612);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_612, L_613);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_614 = V_61;
		V_61 = ((int32_t)il2cpp_codegen_add(L_614, 1));
	}

IL_166d:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_615 = V_61;
		int32_t L_616 = __this->___coin_level_12;
		if ((((int32_t)L_615) <= ((int32_t)L_616)))
		{
			goto IL_163b;
		}
	}
	{
		// coin_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_617 = __this->___coin_txt_14;
		NullCheck(L_617);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_617, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		goto IL_1786;
	}

IL_168c:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_62 = 0;
		goto IL_16c3;
	}

IL_1691:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_618 = __this->___lvl_coin_13;
		int32_t L_619 = V_62;
		NullCheck(L_618);
		int32_t L_620 = L_619;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_621 = (L_618)->GetAt(static_cast<il2cpp_array_size_t>(L_620));
		NullCheck(L_621);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_622;
		L_622 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_621, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_623;
		memset((&L_623), 0, sizeof(L_623));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_623), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_622);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_622, L_623);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_624 = V_62;
		V_62 = ((int32_t)il2cpp_codegen_add(L_624, 1));
	}

IL_16c3:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_625 = V_62;
		int32_t L_626 = __this->___coin_level_12;
		if ((((int32_t)L_625) <= ((int32_t)L_626)))
		{
			goto IL_1691;
		}
	}
	{
		// coin_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_627 = __this->___coin_txt_14;
		NullCheck(L_627);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_627, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		goto IL_1786;
	}

IL_16e2:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_63 = 0;
		goto IL_1719;
	}

IL_16e7:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_628 = __this->___lvl_coin_13;
		int32_t L_629 = V_63;
		NullCheck(L_628);
		int32_t L_630 = L_629;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_631 = (L_628)->GetAt(static_cast<il2cpp_array_size_t>(L_630));
		NullCheck(L_631);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_632;
		L_632 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_631, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_633;
		memset((&L_633), 0, sizeof(L_633));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_633), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_632);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_632, L_633);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_634 = V_63;
		V_63 = ((int32_t)il2cpp_codegen_add(L_634, 1));
	}

IL_1719:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_635 = V_63;
		int32_t L_636 = __this->___coin_level_12;
		if ((((int32_t)L_635) <= ((int32_t)L_636)))
		{
			goto IL_16e7;
		}
	}
	{
		// coin_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_637 = __this->___coin_txt_14;
		NullCheck(L_637);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_637, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		goto IL_1786;
	}

IL_1735:
	{
		// for (int i = 0; i <= coin_level; i++)
		V_64 = 0;
		goto IL_176c;
	}

IL_173a:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_638 = __this->___lvl_coin_13;
		int32_t L_639 = V_64;
		NullCheck(L_638);
		int32_t L_640 = L_639;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_641 = (L_638)->GetAt(static_cast<il2cpp_array_size_t>(L_640));
		NullCheck(L_641);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_642;
		L_642 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_641, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_643;
		memset((&L_643), 0, sizeof(L_643));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_643), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_642);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_642, L_643);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_644 = V_64;
		V_64 = ((int32_t)il2cpp_codegen_add(L_644, 1));
	}

IL_176c:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_645 = V_64;
		int32_t L_646 = __this->___coin_level_12;
		if ((((int32_t)L_645) <= ((int32_t)L_646)))
		{
			goto IL_173a;
		}
	}
	{
		// coin_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_647 = __this->___coin_txt_14;
		NullCheck(L_647);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_647, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
	}

IL_1786:
	{
		// switch (res_level)
		int32_t L_648 = __this->___res_level_9;
		V_4 = L_648;
		int32_t L_649 = V_4;
		switch (L_649)
		{
			case 0:
			{
				goto IL_17ea;
			}
			case 1:
			{
				goto IL_1840;
			}
			case 2:
			{
				goto IL_1896;
			}
			case 3:
			{
				goto IL_18ec;
			}
			case 4:
			{
				goto IL_1942;
			}
			case 5:
			{
				goto IL_1998;
			}
			case 6:
			{
				goto IL_19ee;
			}
			case 7:
			{
				goto IL_1a44;
			}
			case 8:
			{
				goto IL_1a9a;
			}
			case 9:
			{
				goto IL_1af0;
			}
			case 10:
			{
				goto IL_1b46;
			}
			case 11:
			{
				goto IL_1b9c;
			}
			case 12:
			{
				goto IL_1bf2;
			}
			case 13:
			{
				goto IL_1c48;
			}
			case 14:
			{
				goto IL_1c9e;
			}
			case 15:
			{
				goto IL_1cf4;
			}
			case 16:
			{
				goto IL_1d4a;
			}
			case 17:
			{
				goto IL_1da0;
			}
			case 18:
			{
				goto IL_1df6;
			}
			case 19:
			{
				goto IL_1e49;
			}
		}
	}
	{
		goto IL_1e9a;
	}

IL_17ea:
	{
		// for (int i = 0; i <= res_level; i++)
		V_65 = 0;
		goto IL_1821;
	}

IL_17ef:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_650 = __this->___lvl_mul_4;
		int32_t L_651 = V_65;
		NullCheck(L_650);
		int32_t L_652 = L_651;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_653 = (L_650)->GetAt(static_cast<il2cpp_array_size_t>(L_652));
		NullCheck(L_653);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_654;
		L_654 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_653, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_655;
		memset((&L_655), 0, sizeof(L_655));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_655), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_654);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_654, L_655);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_656 = V_65;
		V_65 = ((int32_t)il2cpp_codegen_add(L_656, 1));
	}

IL_1821:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_657 = V_65;
		int32_t L_658 = __this->___res_level_9;
		if ((((int32_t)L_657) <= ((int32_t)L_658)))
		{
			goto IL_17ef;
		}
	}
	{
		// res_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_659 = __this->___res_txt_11;
		NullCheck(L_659);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_659, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		goto IL_1e9a;
	}

IL_1840:
	{
		// for (int i = 0; i <= res_level; i++)
		V_66 = 0;
		goto IL_1877;
	}

IL_1845:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_660 = __this->___lvl_mul_4;
		int32_t L_661 = V_66;
		NullCheck(L_660);
		int32_t L_662 = L_661;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_663 = (L_660)->GetAt(static_cast<il2cpp_array_size_t>(L_662));
		NullCheck(L_663);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_664;
		L_664 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_663, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_665;
		memset((&L_665), 0, sizeof(L_665));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_665), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_664);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_664, L_665);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_666 = V_66;
		V_66 = ((int32_t)il2cpp_codegen_add(L_666, 1));
	}

IL_1877:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_667 = V_66;
		int32_t L_668 = __this->___res_level_9;
		if ((((int32_t)L_667) <= ((int32_t)L_668)))
		{
			goto IL_1845;
		}
	}
	{
		// res_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_669 = __this->___res_txt_11;
		NullCheck(L_669);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_669, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		goto IL_1e9a;
	}

IL_1896:
	{
		// for (int i = 0; i <= res_level; i++)
		V_67 = 0;
		goto IL_18cd;
	}

IL_189b:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_670 = __this->___lvl_mul_4;
		int32_t L_671 = V_67;
		NullCheck(L_670);
		int32_t L_672 = L_671;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_673 = (L_670)->GetAt(static_cast<il2cpp_array_size_t>(L_672));
		NullCheck(L_673);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_674;
		L_674 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_673, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_675;
		memset((&L_675), 0, sizeof(L_675));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_675), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_674);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_674, L_675);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_676 = V_67;
		V_67 = ((int32_t)il2cpp_codegen_add(L_676, 1));
	}

IL_18cd:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_677 = V_67;
		int32_t L_678 = __this->___res_level_9;
		if ((((int32_t)L_677) <= ((int32_t)L_678)))
		{
			goto IL_189b;
		}
	}
	{
		// res_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_679 = __this->___res_txt_11;
		NullCheck(L_679);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_679, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		goto IL_1e9a;
	}

IL_18ec:
	{
		// for (int i = 0; i <= res_level; i++)
		V_68 = 0;
		goto IL_1923;
	}

IL_18f1:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_680 = __this->___lvl_mul_4;
		int32_t L_681 = V_68;
		NullCheck(L_680);
		int32_t L_682 = L_681;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_683 = (L_680)->GetAt(static_cast<il2cpp_array_size_t>(L_682));
		NullCheck(L_683);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_684;
		L_684 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_683, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_685;
		memset((&L_685), 0, sizeof(L_685));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_685), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_684);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_684, L_685);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_686 = V_68;
		V_68 = ((int32_t)il2cpp_codegen_add(L_686, 1));
	}

IL_1923:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_687 = V_68;
		int32_t L_688 = __this->___res_level_9;
		if ((((int32_t)L_687) <= ((int32_t)L_688)))
		{
			goto IL_18f1;
		}
	}
	{
		// res_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_689 = __this->___res_txt_11;
		NullCheck(L_689);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_689, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		goto IL_1e9a;
	}

IL_1942:
	{
		// for (int i = 0; i <= res_level; i++)
		V_69 = 0;
		goto IL_1979;
	}

IL_1947:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_690 = __this->___lvl_mul_4;
		int32_t L_691 = V_69;
		NullCheck(L_690);
		int32_t L_692 = L_691;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_693 = (L_690)->GetAt(static_cast<il2cpp_array_size_t>(L_692));
		NullCheck(L_693);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_694;
		L_694 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_693, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_695;
		memset((&L_695), 0, sizeof(L_695));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_695), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_694);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_694, L_695);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_696 = V_69;
		V_69 = ((int32_t)il2cpp_codegen_add(L_696, 1));
	}

IL_1979:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_697 = V_69;
		int32_t L_698 = __this->___res_level_9;
		if ((((int32_t)L_697) <= ((int32_t)L_698)))
		{
			goto IL_1947;
		}
	}
	{
		// res_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_699 = __this->___res_txt_11;
		NullCheck(L_699);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_699, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		goto IL_1e9a;
	}

IL_1998:
	{
		// for (int i = 0; i <= res_level; i++)
		V_70 = 0;
		goto IL_19cf;
	}

IL_199d:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_700 = __this->___lvl_mul_4;
		int32_t L_701 = V_70;
		NullCheck(L_700);
		int32_t L_702 = L_701;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_703 = (L_700)->GetAt(static_cast<il2cpp_array_size_t>(L_702));
		NullCheck(L_703);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_704;
		L_704 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_703, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_705;
		memset((&L_705), 0, sizeof(L_705));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_705), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_704);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_704, L_705);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_706 = V_70;
		V_70 = ((int32_t)il2cpp_codegen_add(L_706, 1));
	}

IL_19cf:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_707 = V_70;
		int32_t L_708 = __this->___res_level_9;
		if ((((int32_t)L_707) <= ((int32_t)L_708)))
		{
			goto IL_199d;
		}
	}
	{
		// res_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_709 = __this->___res_txt_11;
		NullCheck(L_709);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_709, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		goto IL_1e9a;
	}

IL_19ee:
	{
		// for (int i = 0; i <= res_level; i++)
		V_71 = 0;
		goto IL_1a25;
	}

IL_19f3:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_710 = __this->___lvl_mul_4;
		int32_t L_711 = V_71;
		NullCheck(L_710);
		int32_t L_712 = L_711;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_713 = (L_710)->GetAt(static_cast<il2cpp_array_size_t>(L_712));
		NullCheck(L_713);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_714;
		L_714 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_713, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_715;
		memset((&L_715), 0, sizeof(L_715));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_715), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_714);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_714, L_715);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_716 = V_71;
		V_71 = ((int32_t)il2cpp_codegen_add(L_716, 1));
	}

IL_1a25:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_717 = V_71;
		int32_t L_718 = __this->___res_level_9;
		if ((((int32_t)L_717) <= ((int32_t)L_718)))
		{
			goto IL_19f3;
		}
	}
	{
		// res_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_719 = __this->___res_txt_11;
		NullCheck(L_719);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_719, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		goto IL_1e9a;
	}

IL_1a44:
	{
		// for (int i = 0; i <= res_level; i++)
		V_72 = 0;
		goto IL_1a7b;
	}

IL_1a49:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_720 = __this->___lvl_mul_4;
		int32_t L_721 = V_72;
		NullCheck(L_720);
		int32_t L_722 = L_721;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_723 = (L_720)->GetAt(static_cast<il2cpp_array_size_t>(L_722));
		NullCheck(L_723);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_724;
		L_724 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_723, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_725;
		memset((&L_725), 0, sizeof(L_725));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_725), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_724);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_724, L_725);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_726 = V_72;
		V_72 = ((int32_t)il2cpp_codegen_add(L_726, 1));
	}

IL_1a7b:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_727 = V_72;
		int32_t L_728 = __this->___res_level_9;
		if ((((int32_t)L_727) <= ((int32_t)L_728)))
		{
			goto IL_1a49;
		}
	}
	{
		// res_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_729 = __this->___res_txt_11;
		NullCheck(L_729);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_729, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		goto IL_1e9a;
	}

IL_1a9a:
	{
		// for (int i = 0; i <= res_level; i++)
		V_73 = 0;
		goto IL_1ad1;
	}

IL_1a9f:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_730 = __this->___lvl_mul_4;
		int32_t L_731 = V_73;
		NullCheck(L_730);
		int32_t L_732 = L_731;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_733 = (L_730)->GetAt(static_cast<il2cpp_array_size_t>(L_732));
		NullCheck(L_733);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_734;
		L_734 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_733, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_735;
		memset((&L_735), 0, sizeof(L_735));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_735), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_734);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_734, L_735);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_736 = V_73;
		V_73 = ((int32_t)il2cpp_codegen_add(L_736, 1));
	}

IL_1ad1:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_737 = V_73;
		int32_t L_738 = __this->___res_level_9;
		if ((((int32_t)L_737) <= ((int32_t)L_738)))
		{
			goto IL_1a9f;
		}
	}
	{
		// res_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_739 = __this->___res_txt_11;
		NullCheck(L_739);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_739, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		goto IL_1e9a;
	}

IL_1af0:
	{
		// for (int i = 0; i <= res_level; i++)
		V_74 = 0;
		goto IL_1b27;
	}

IL_1af5:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_740 = __this->___lvl_mul_4;
		int32_t L_741 = V_74;
		NullCheck(L_740);
		int32_t L_742 = L_741;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_743 = (L_740)->GetAt(static_cast<il2cpp_array_size_t>(L_742));
		NullCheck(L_743);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_744;
		L_744 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_743, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_745;
		memset((&L_745), 0, sizeof(L_745));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_745), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_744);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_744, L_745);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_746 = V_74;
		V_74 = ((int32_t)il2cpp_codegen_add(L_746, 1));
	}

IL_1b27:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_747 = V_74;
		int32_t L_748 = __this->___res_level_9;
		if ((((int32_t)L_747) <= ((int32_t)L_748)))
		{
			goto IL_1af5;
		}
	}
	{
		// res_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_749 = __this->___res_txt_11;
		NullCheck(L_749);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_749, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		goto IL_1e9a;
	}

IL_1b46:
	{
		// for (int i = 0; i <= res_level; i++)
		V_75 = 0;
		goto IL_1b7d;
	}

IL_1b4b:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_750 = __this->___lvl_mul_4;
		int32_t L_751 = V_75;
		NullCheck(L_750);
		int32_t L_752 = L_751;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_753 = (L_750)->GetAt(static_cast<il2cpp_array_size_t>(L_752));
		NullCheck(L_753);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_754;
		L_754 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_753, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_755;
		memset((&L_755), 0, sizeof(L_755));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_755), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_754);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_754, L_755);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_756 = V_75;
		V_75 = ((int32_t)il2cpp_codegen_add(L_756, 1));
	}

IL_1b7d:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_757 = V_75;
		int32_t L_758 = __this->___res_level_9;
		if ((((int32_t)L_757) <= ((int32_t)L_758)))
		{
			goto IL_1b4b;
		}
	}
	{
		// res_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_759 = __this->___res_txt_11;
		NullCheck(L_759);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_759, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		goto IL_1e9a;
	}

IL_1b9c:
	{
		// for (int i = 0; i <= res_level; i++)
		V_76 = 0;
		goto IL_1bd3;
	}

IL_1ba1:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_760 = __this->___lvl_mul_4;
		int32_t L_761 = V_76;
		NullCheck(L_760);
		int32_t L_762 = L_761;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_763 = (L_760)->GetAt(static_cast<il2cpp_array_size_t>(L_762));
		NullCheck(L_763);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_764;
		L_764 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_763, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_765;
		memset((&L_765), 0, sizeof(L_765));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_765), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_764);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_764, L_765);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_766 = V_76;
		V_76 = ((int32_t)il2cpp_codegen_add(L_766, 1));
	}

IL_1bd3:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_767 = V_76;
		int32_t L_768 = __this->___res_level_9;
		if ((((int32_t)L_767) <= ((int32_t)L_768)))
		{
			goto IL_1ba1;
		}
	}
	{
		// res_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_769 = __this->___res_txt_11;
		NullCheck(L_769);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_769, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		goto IL_1e9a;
	}

IL_1bf2:
	{
		// for (int i = 0; i <= res_level; i++)
		V_77 = 0;
		goto IL_1c29;
	}

IL_1bf7:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_770 = __this->___lvl_mul_4;
		int32_t L_771 = V_77;
		NullCheck(L_770);
		int32_t L_772 = L_771;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_773 = (L_770)->GetAt(static_cast<il2cpp_array_size_t>(L_772));
		NullCheck(L_773);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_774;
		L_774 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_773, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_775;
		memset((&L_775), 0, sizeof(L_775));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_775), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_774);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_774, L_775);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_776 = V_77;
		V_77 = ((int32_t)il2cpp_codegen_add(L_776, 1));
	}

IL_1c29:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_777 = V_77;
		int32_t L_778 = __this->___res_level_9;
		if ((((int32_t)L_777) <= ((int32_t)L_778)))
		{
			goto IL_1bf7;
		}
	}
	{
		// res_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_779 = __this->___res_txt_11;
		NullCheck(L_779);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_779, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		goto IL_1e9a;
	}

IL_1c48:
	{
		// for (int i = 0; i <= res_level; i++)
		V_78 = 0;
		goto IL_1c7f;
	}

IL_1c4d:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_780 = __this->___lvl_mul_4;
		int32_t L_781 = V_78;
		NullCheck(L_780);
		int32_t L_782 = L_781;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_783 = (L_780)->GetAt(static_cast<il2cpp_array_size_t>(L_782));
		NullCheck(L_783);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_784;
		L_784 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_783, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_785;
		memset((&L_785), 0, sizeof(L_785));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_785), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_784);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_784, L_785);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_786 = V_78;
		V_78 = ((int32_t)il2cpp_codegen_add(L_786, 1));
	}

IL_1c7f:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_787 = V_78;
		int32_t L_788 = __this->___res_level_9;
		if ((((int32_t)L_787) <= ((int32_t)L_788)))
		{
			goto IL_1c4d;
		}
	}
	{
		// res_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_789 = __this->___res_txt_11;
		NullCheck(L_789);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_789, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		goto IL_1e9a;
	}

IL_1c9e:
	{
		// for (int i = 0; i <= res_level; i++)
		V_79 = 0;
		goto IL_1cd5;
	}

IL_1ca3:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_790 = __this->___lvl_mul_4;
		int32_t L_791 = V_79;
		NullCheck(L_790);
		int32_t L_792 = L_791;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_793 = (L_790)->GetAt(static_cast<il2cpp_array_size_t>(L_792));
		NullCheck(L_793);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_794;
		L_794 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_793, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_795;
		memset((&L_795), 0, sizeof(L_795));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_795), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_794);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_794, L_795);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_796 = V_79;
		V_79 = ((int32_t)il2cpp_codegen_add(L_796, 1));
	}

IL_1cd5:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_797 = V_79;
		int32_t L_798 = __this->___res_level_9;
		if ((((int32_t)L_797) <= ((int32_t)L_798)))
		{
			goto IL_1ca3;
		}
	}
	{
		// res_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_799 = __this->___res_txt_11;
		NullCheck(L_799);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_799, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		goto IL_1e9a;
	}

IL_1cf4:
	{
		// for (int i = 0; i <= res_level; i++)
		V_80 = 0;
		goto IL_1d2b;
	}

IL_1cf9:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_800 = __this->___lvl_mul_4;
		int32_t L_801 = V_80;
		NullCheck(L_800);
		int32_t L_802 = L_801;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_803 = (L_800)->GetAt(static_cast<il2cpp_array_size_t>(L_802));
		NullCheck(L_803);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_804;
		L_804 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_803, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_805;
		memset((&L_805), 0, sizeof(L_805));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_805), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_804);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_804, L_805);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_806 = V_80;
		V_80 = ((int32_t)il2cpp_codegen_add(L_806, 1));
	}

IL_1d2b:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_807 = V_80;
		int32_t L_808 = __this->___res_level_9;
		if ((((int32_t)L_807) <= ((int32_t)L_808)))
		{
			goto IL_1cf9;
		}
	}
	{
		// res_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_809 = __this->___res_txt_11;
		NullCheck(L_809);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_809, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		goto IL_1e9a;
	}

IL_1d4a:
	{
		// for (int i = 0; i <= res_level; i++)
		V_81 = 0;
		goto IL_1d81;
	}

IL_1d4f:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_810 = __this->___lvl_mul_4;
		int32_t L_811 = V_81;
		NullCheck(L_810);
		int32_t L_812 = L_811;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_813 = (L_810)->GetAt(static_cast<il2cpp_array_size_t>(L_812));
		NullCheck(L_813);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_814;
		L_814 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_813, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_815;
		memset((&L_815), 0, sizeof(L_815));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_815), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_814);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_814, L_815);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_816 = V_81;
		V_81 = ((int32_t)il2cpp_codegen_add(L_816, 1));
	}

IL_1d81:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_817 = V_81;
		int32_t L_818 = __this->___res_level_9;
		if ((((int32_t)L_817) <= ((int32_t)L_818)))
		{
			goto IL_1d4f;
		}
	}
	{
		// res_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_819 = __this->___res_txt_11;
		NullCheck(L_819);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_819, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		goto IL_1e9a;
	}

IL_1da0:
	{
		// for (int i = 0; i <= res_level; i++)
		V_82 = 0;
		goto IL_1dd7;
	}

IL_1da5:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_820 = __this->___lvl_mul_4;
		int32_t L_821 = V_82;
		NullCheck(L_820);
		int32_t L_822 = L_821;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_823 = (L_820)->GetAt(static_cast<il2cpp_array_size_t>(L_822));
		NullCheck(L_823);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_824;
		L_824 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_823, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_825;
		memset((&L_825), 0, sizeof(L_825));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_825), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_824);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_824, L_825);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_826 = V_82;
		V_82 = ((int32_t)il2cpp_codegen_add(L_826, 1));
	}

IL_1dd7:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_827 = V_82;
		int32_t L_828 = __this->___res_level_9;
		if ((((int32_t)L_827) <= ((int32_t)L_828)))
		{
			goto IL_1da5;
		}
	}
	{
		// res_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_829 = __this->___res_txt_11;
		NullCheck(L_829);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_829, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		goto IL_1e9a;
	}

IL_1df6:
	{
		// for (int i = 0; i <= res_level; i++)
		V_83 = 0;
		goto IL_1e2d;
	}

IL_1dfb:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_830 = __this->___lvl_mul_4;
		int32_t L_831 = V_83;
		NullCheck(L_830);
		int32_t L_832 = L_831;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_833 = (L_830)->GetAt(static_cast<il2cpp_array_size_t>(L_832));
		NullCheck(L_833);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_834;
		L_834 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_833, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_835;
		memset((&L_835), 0, sizeof(L_835));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_835), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_834);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_834, L_835);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_836 = V_83;
		V_83 = ((int32_t)il2cpp_codegen_add(L_836, 1));
	}

IL_1e2d:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_837 = V_83;
		int32_t L_838 = __this->___res_level_9;
		if ((((int32_t)L_837) <= ((int32_t)L_838)))
		{
			goto IL_1dfb;
		}
	}
	{
		// res_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_839 = __this->___res_txt_11;
		NullCheck(L_839);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_839, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		goto IL_1e9a;
	}

IL_1e49:
	{
		// for (int i = 0; i <= res_level; i++)
		V_84 = 0;
		goto IL_1e80;
	}

IL_1e4e:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_840 = __this->___lvl_mul_4;
		int32_t L_841 = V_84;
		NullCheck(L_840);
		int32_t L_842 = L_841;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_843 = (L_840)->GetAt(static_cast<il2cpp_array_size_t>(L_842));
		NullCheck(L_843);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_844;
		L_844 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_843, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_845;
		memset((&L_845), 0, sizeof(L_845));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_845), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_844);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_844, L_845);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_846 = V_84;
		V_84 = ((int32_t)il2cpp_codegen_add(L_846, 1));
	}

IL_1e80:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_847 = V_84;
		int32_t L_848 = __this->___res_level_9;
		if ((((int32_t)L_847) <= ((int32_t)L_848)))
		{
			goto IL_1e4e;
		}
	}
	{
		// res_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_849 = __this->___res_txt_11;
		NullCheck(L_849);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_849, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
	}

IL_1e9a:
	{
		// switch (scr_lvl)
		int32_t L_850 = __this->___scr_lvl_21;
		V_4 = L_850;
		int32_t L_851 = V_4;
		switch (L_851)
		{
			case 0:
			{
				goto IL_1efe;
			}
			case 1:
			{
				goto IL_1f54;
			}
			case 2:
			{
				goto IL_1faa;
			}
			case 3:
			{
				goto IL_2000;
			}
			case 4:
			{
				goto IL_2056;
			}
			case 5:
			{
				goto IL_20ac;
			}
			case 6:
			{
				goto IL_2102;
			}
			case 7:
			{
				goto IL_2158;
			}
			case 8:
			{
				goto IL_21ae;
			}
			case 9:
			{
				goto IL_2204;
			}
			case 10:
			{
				goto IL_225a;
			}
			case 11:
			{
				goto IL_22b0;
			}
			case 12:
			{
				goto IL_2306;
			}
			case 13:
			{
				goto IL_235c;
			}
			case 14:
			{
				goto IL_23b2;
			}
			case 15:
			{
				goto IL_2408;
			}
			case 16:
			{
				goto IL_245e;
			}
			case 17:
			{
				goto IL_24b4;
			}
			case 18:
			{
				goto IL_250a;
			}
			case 19:
			{
				goto IL_255d;
			}
		}
	}
	{
		goto IL_25ae;
	}

IL_1efe:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_85 = 0;
		goto IL_1f35;
	}

IL_1f03:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_852 = __this->___lvl_scr_22;
		int32_t L_853 = V_85;
		NullCheck(L_852);
		int32_t L_854 = L_853;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_855 = (L_852)->GetAt(static_cast<il2cpp_array_size_t>(L_854));
		NullCheck(L_855);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_856;
		L_856 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_855, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_857;
		memset((&L_857), 0, sizeof(L_857));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_857), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_856);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_856, L_857);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_858 = V_85;
		V_85 = ((int32_t)il2cpp_codegen_add(L_858, 1));
	}

IL_1f35:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_859 = V_85;
		int32_t L_860 = __this->___scr_lvl_21;
		if ((((int32_t)L_859) <= ((int32_t)L_860)))
		{
			goto IL_1f03;
		}
	}
	{
		// scr_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_861 = __this->___scr_txt_23;
		NullCheck(L_861);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_861, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		goto IL_25ae;
	}

IL_1f54:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_86 = 0;
		goto IL_1f8b;
	}

IL_1f59:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_862 = __this->___lvl_scr_22;
		int32_t L_863 = V_86;
		NullCheck(L_862);
		int32_t L_864 = L_863;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_865 = (L_862)->GetAt(static_cast<il2cpp_array_size_t>(L_864));
		NullCheck(L_865);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_866;
		L_866 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_865, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_867;
		memset((&L_867), 0, sizeof(L_867));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_867), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_866);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_866, L_867);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_868 = V_86;
		V_86 = ((int32_t)il2cpp_codegen_add(L_868, 1));
	}

IL_1f8b:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_869 = V_86;
		int32_t L_870 = __this->___scr_lvl_21;
		if ((((int32_t)L_869) <= ((int32_t)L_870)))
		{
			goto IL_1f59;
		}
	}
	{
		// scr_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_871 = __this->___scr_txt_23;
		NullCheck(L_871);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_871, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		goto IL_25ae;
	}

IL_1faa:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_87 = 0;
		goto IL_1fe1;
	}

IL_1faf:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_872 = __this->___lvl_scr_22;
		int32_t L_873 = V_87;
		NullCheck(L_872);
		int32_t L_874 = L_873;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_875 = (L_872)->GetAt(static_cast<il2cpp_array_size_t>(L_874));
		NullCheck(L_875);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_876;
		L_876 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_875, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_877;
		memset((&L_877), 0, sizeof(L_877));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_877), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_876);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_876, L_877);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_878 = V_87;
		V_87 = ((int32_t)il2cpp_codegen_add(L_878, 1));
	}

IL_1fe1:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_879 = V_87;
		int32_t L_880 = __this->___scr_lvl_21;
		if ((((int32_t)L_879) <= ((int32_t)L_880)))
		{
			goto IL_1faf;
		}
	}
	{
		// scr_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_881 = __this->___scr_txt_23;
		NullCheck(L_881);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_881, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		goto IL_25ae;
	}

IL_2000:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_88 = 0;
		goto IL_2037;
	}

IL_2005:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_882 = __this->___lvl_scr_22;
		int32_t L_883 = V_88;
		NullCheck(L_882);
		int32_t L_884 = L_883;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_885 = (L_882)->GetAt(static_cast<il2cpp_array_size_t>(L_884));
		NullCheck(L_885);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_886;
		L_886 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_885, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_887;
		memset((&L_887), 0, sizeof(L_887));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_887), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_886);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_886, L_887);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_888 = V_88;
		V_88 = ((int32_t)il2cpp_codegen_add(L_888, 1));
	}

IL_2037:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_889 = V_88;
		int32_t L_890 = __this->___scr_lvl_21;
		if ((((int32_t)L_889) <= ((int32_t)L_890)))
		{
			goto IL_2005;
		}
	}
	{
		// scr_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_891 = __this->___scr_txt_23;
		NullCheck(L_891);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_891, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		goto IL_25ae;
	}

IL_2056:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_89 = 0;
		goto IL_208d;
	}

IL_205b:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_892 = __this->___lvl_scr_22;
		int32_t L_893 = V_89;
		NullCheck(L_892);
		int32_t L_894 = L_893;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_895 = (L_892)->GetAt(static_cast<il2cpp_array_size_t>(L_894));
		NullCheck(L_895);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_896;
		L_896 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_895, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_897;
		memset((&L_897), 0, sizeof(L_897));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_897), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_896);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_896, L_897);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_898 = V_89;
		V_89 = ((int32_t)il2cpp_codegen_add(L_898, 1));
	}

IL_208d:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_899 = V_89;
		int32_t L_900 = __this->___scr_lvl_21;
		if ((((int32_t)L_899) <= ((int32_t)L_900)))
		{
			goto IL_205b;
		}
	}
	{
		// scr_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_901 = __this->___scr_txt_23;
		NullCheck(L_901);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_901, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		goto IL_25ae;
	}

IL_20ac:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_90 = 0;
		goto IL_20e3;
	}

IL_20b1:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_902 = __this->___lvl_scr_22;
		int32_t L_903 = V_90;
		NullCheck(L_902);
		int32_t L_904 = L_903;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_905 = (L_902)->GetAt(static_cast<il2cpp_array_size_t>(L_904));
		NullCheck(L_905);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_906;
		L_906 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_905, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_907;
		memset((&L_907), 0, sizeof(L_907));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_907), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_906);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_906, L_907);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_908 = V_90;
		V_90 = ((int32_t)il2cpp_codegen_add(L_908, 1));
	}

IL_20e3:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_909 = V_90;
		int32_t L_910 = __this->___scr_lvl_21;
		if ((((int32_t)L_909) <= ((int32_t)L_910)))
		{
			goto IL_20b1;
		}
	}
	{
		// scr_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_911 = __this->___scr_txt_23;
		NullCheck(L_911);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_911, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		goto IL_25ae;
	}

IL_2102:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_91 = 0;
		goto IL_2139;
	}

IL_2107:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_912 = __this->___lvl_scr_22;
		int32_t L_913 = V_91;
		NullCheck(L_912);
		int32_t L_914 = L_913;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_915 = (L_912)->GetAt(static_cast<il2cpp_array_size_t>(L_914));
		NullCheck(L_915);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_916;
		L_916 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_915, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_917;
		memset((&L_917), 0, sizeof(L_917));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_917), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_916);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_916, L_917);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_918 = V_91;
		V_91 = ((int32_t)il2cpp_codegen_add(L_918, 1));
	}

IL_2139:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_919 = V_91;
		int32_t L_920 = __this->___scr_lvl_21;
		if ((((int32_t)L_919) <= ((int32_t)L_920)))
		{
			goto IL_2107;
		}
	}
	{
		// scr_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_921 = __this->___scr_txt_23;
		NullCheck(L_921);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_921, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		goto IL_25ae;
	}

IL_2158:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_92 = 0;
		goto IL_218f;
	}

IL_215d:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_922 = __this->___lvl_scr_22;
		int32_t L_923 = V_92;
		NullCheck(L_922);
		int32_t L_924 = L_923;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_925 = (L_922)->GetAt(static_cast<il2cpp_array_size_t>(L_924));
		NullCheck(L_925);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_926;
		L_926 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_925, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_927;
		memset((&L_927), 0, sizeof(L_927));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_927), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_926);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_926, L_927);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_928 = V_92;
		V_92 = ((int32_t)il2cpp_codegen_add(L_928, 1));
	}

IL_218f:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_929 = V_92;
		int32_t L_930 = __this->___scr_lvl_21;
		if ((((int32_t)L_929) <= ((int32_t)L_930)))
		{
			goto IL_215d;
		}
	}
	{
		// scr_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_931 = __this->___scr_txt_23;
		NullCheck(L_931);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_931, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		goto IL_25ae;
	}

IL_21ae:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_93 = 0;
		goto IL_21e5;
	}

IL_21b3:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_932 = __this->___lvl_scr_22;
		int32_t L_933 = V_93;
		NullCheck(L_932);
		int32_t L_934 = L_933;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_935 = (L_932)->GetAt(static_cast<il2cpp_array_size_t>(L_934));
		NullCheck(L_935);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_936;
		L_936 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_935, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_937;
		memset((&L_937), 0, sizeof(L_937));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_937), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_936);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_936, L_937);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_938 = V_93;
		V_93 = ((int32_t)il2cpp_codegen_add(L_938, 1));
	}

IL_21e5:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_939 = V_93;
		int32_t L_940 = __this->___scr_lvl_21;
		if ((((int32_t)L_939) <= ((int32_t)L_940)))
		{
			goto IL_21b3;
		}
	}
	{
		// scr_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_941 = __this->___scr_txt_23;
		NullCheck(L_941);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_941, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		goto IL_25ae;
	}

IL_2204:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_94 = 0;
		goto IL_223b;
	}

IL_2209:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_942 = __this->___lvl_scr_22;
		int32_t L_943 = V_94;
		NullCheck(L_942);
		int32_t L_944 = L_943;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_945 = (L_942)->GetAt(static_cast<il2cpp_array_size_t>(L_944));
		NullCheck(L_945);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_946;
		L_946 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_945, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_947;
		memset((&L_947), 0, sizeof(L_947));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_947), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_946);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_946, L_947);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_948 = V_94;
		V_94 = ((int32_t)il2cpp_codegen_add(L_948, 1));
	}

IL_223b:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_949 = V_94;
		int32_t L_950 = __this->___scr_lvl_21;
		if ((((int32_t)L_949) <= ((int32_t)L_950)))
		{
			goto IL_2209;
		}
	}
	{
		// scr_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_951 = __this->___scr_txt_23;
		NullCheck(L_951);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_951, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		goto IL_25ae;
	}

IL_225a:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_95 = 0;
		goto IL_2291;
	}

IL_225f:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_952 = __this->___lvl_scr_22;
		int32_t L_953 = V_95;
		NullCheck(L_952);
		int32_t L_954 = L_953;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_955 = (L_952)->GetAt(static_cast<il2cpp_array_size_t>(L_954));
		NullCheck(L_955);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_956;
		L_956 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_955, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_957;
		memset((&L_957), 0, sizeof(L_957));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_957), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_956);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_956, L_957);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_958 = V_95;
		V_95 = ((int32_t)il2cpp_codegen_add(L_958, 1));
	}

IL_2291:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_959 = V_95;
		int32_t L_960 = __this->___scr_lvl_21;
		if ((((int32_t)L_959) <= ((int32_t)L_960)))
		{
			goto IL_225f;
		}
	}
	{
		// scr_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_961 = __this->___scr_txt_23;
		NullCheck(L_961);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_961, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		goto IL_25ae;
	}

IL_22b0:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_96 = 0;
		goto IL_22e7;
	}

IL_22b5:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_962 = __this->___lvl_scr_22;
		int32_t L_963 = V_96;
		NullCheck(L_962);
		int32_t L_964 = L_963;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_965 = (L_962)->GetAt(static_cast<il2cpp_array_size_t>(L_964));
		NullCheck(L_965);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_966;
		L_966 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_965, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_967;
		memset((&L_967), 0, sizeof(L_967));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_967), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_966);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_966, L_967);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_968 = V_96;
		V_96 = ((int32_t)il2cpp_codegen_add(L_968, 1));
	}

IL_22e7:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_969 = V_96;
		int32_t L_970 = __this->___scr_lvl_21;
		if ((((int32_t)L_969) <= ((int32_t)L_970)))
		{
			goto IL_22b5;
		}
	}
	{
		// scr_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_971 = __this->___scr_txt_23;
		NullCheck(L_971);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_971, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		goto IL_25ae;
	}

IL_2306:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_97 = 0;
		goto IL_233d;
	}

IL_230b:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_972 = __this->___lvl_scr_22;
		int32_t L_973 = V_97;
		NullCheck(L_972);
		int32_t L_974 = L_973;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_975 = (L_972)->GetAt(static_cast<il2cpp_array_size_t>(L_974));
		NullCheck(L_975);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_976;
		L_976 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_975, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_977;
		memset((&L_977), 0, sizeof(L_977));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_977), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_976);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_976, L_977);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_978 = V_97;
		V_97 = ((int32_t)il2cpp_codegen_add(L_978, 1));
	}

IL_233d:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_979 = V_97;
		int32_t L_980 = __this->___scr_lvl_21;
		if ((((int32_t)L_979) <= ((int32_t)L_980)))
		{
			goto IL_230b;
		}
	}
	{
		// scr_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_981 = __this->___scr_txt_23;
		NullCheck(L_981);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_981, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		goto IL_25ae;
	}

IL_235c:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_98 = 0;
		goto IL_2393;
	}

IL_2361:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_982 = __this->___lvl_scr_22;
		int32_t L_983 = V_98;
		NullCheck(L_982);
		int32_t L_984 = L_983;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_985 = (L_982)->GetAt(static_cast<il2cpp_array_size_t>(L_984));
		NullCheck(L_985);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_986;
		L_986 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_985, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_987;
		memset((&L_987), 0, sizeof(L_987));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_987), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_986);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_986, L_987);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_988 = V_98;
		V_98 = ((int32_t)il2cpp_codegen_add(L_988, 1));
	}

IL_2393:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_989 = V_98;
		int32_t L_990 = __this->___scr_lvl_21;
		if ((((int32_t)L_989) <= ((int32_t)L_990)))
		{
			goto IL_2361;
		}
	}
	{
		// scr_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_991 = __this->___scr_txt_23;
		NullCheck(L_991);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_991, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		goto IL_25ae;
	}

IL_23b2:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_99 = 0;
		goto IL_23e9;
	}

IL_23b7:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_992 = __this->___lvl_scr_22;
		int32_t L_993 = V_99;
		NullCheck(L_992);
		int32_t L_994 = L_993;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_995 = (L_992)->GetAt(static_cast<il2cpp_array_size_t>(L_994));
		NullCheck(L_995);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_996;
		L_996 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_995, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_997;
		memset((&L_997), 0, sizeof(L_997));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_997), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_996);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_996, L_997);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_998 = V_99;
		V_99 = ((int32_t)il2cpp_codegen_add(L_998, 1));
	}

IL_23e9:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_999 = V_99;
		int32_t L_1000 = __this->___scr_lvl_21;
		if ((((int32_t)L_999) <= ((int32_t)L_1000)))
		{
			goto IL_23b7;
		}
	}
	{
		// scr_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1001 = __this->___scr_txt_23;
		NullCheck(L_1001);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1001, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		goto IL_25ae;
	}

IL_2408:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_100 = 0;
		goto IL_243f;
	}

IL_240d:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1002 = __this->___lvl_scr_22;
		int32_t L_1003 = V_100;
		NullCheck(L_1002);
		int32_t L_1004 = L_1003;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1005 = (L_1002)->GetAt(static_cast<il2cpp_array_size_t>(L_1004));
		NullCheck(L_1005);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1006;
		L_1006 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1005, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1007;
		memset((&L_1007), 0, sizeof(L_1007));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1007), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1006);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1006, L_1007);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1008 = V_100;
		V_100 = ((int32_t)il2cpp_codegen_add(L_1008, 1));
	}

IL_243f:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1009 = V_100;
		int32_t L_1010 = __this->___scr_lvl_21;
		if ((((int32_t)L_1009) <= ((int32_t)L_1010)))
		{
			goto IL_240d;
		}
	}
	{
		// scr_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1011 = __this->___scr_txt_23;
		NullCheck(L_1011);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1011, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		goto IL_25ae;
	}

IL_245e:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_101 = 0;
		goto IL_2495;
	}

IL_2463:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1012 = __this->___lvl_scr_22;
		int32_t L_1013 = V_101;
		NullCheck(L_1012);
		int32_t L_1014 = L_1013;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1015 = (L_1012)->GetAt(static_cast<il2cpp_array_size_t>(L_1014));
		NullCheck(L_1015);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1016;
		L_1016 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1015, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1017;
		memset((&L_1017), 0, sizeof(L_1017));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1017), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1016);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1016, L_1017);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1018 = V_101;
		V_101 = ((int32_t)il2cpp_codegen_add(L_1018, 1));
	}

IL_2495:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1019 = V_101;
		int32_t L_1020 = __this->___scr_lvl_21;
		if ((((int32_t)L_1019) <= ((int32_t)L_1020)))
		{
			goto IL_2463;
		}
	}
	{
		// scr_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1021 = __this->___scr_txt_23;
		NullCheck(L_1021);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1021, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		goto IL_25ae;
	}

IL_24b4:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_102 = 0;
		goto IL_24eb;
	}

IL_24b9:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1022 = __this->___lvl_scr_22;
		int32_t L_1023 = V_102;
		NullCheck(L_1022);
		int32_t L_1024 = L_1023;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1025 = (L_1022)->GetAt(static_cast<il2cpp_array_size_t>(L_1024));
		NullCheck(L_1025);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1026;
		L_1026 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1025, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1027;
		memset((&L_1027), 0, sizeof(L_1027));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1027), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1026);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1026, L_1027);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1028 = V_102;
		V_102 = ((int32_t)il2cpp_codegen_add(L_1028, 1));
	}

IL_24eb:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1029 = V_102;
		int32_t L_1030 = __this->___scr_lvl_21;
		if ((((int32_t)L_1029) <= ((int32_t)L_1030)))
		{
			goto IL_24b9;
		}
	}
	{
		// scr_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1031 = __this->___scr_txt_23;
		NullCheck(L_1031);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1031, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		goto IL_25ae;
	}

IL_250a:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_103 = 0;
		goto IL_2541;
	}

IL_250f:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1032 = __this->___lvl_scr_22;
		int32_t L_1033 = V_103;
		NullCheck(L_1032);
		int32_t L_1034 = L_1033;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1035 = (L_1032)->GetAt(static_cast<il2cpp_array_size_t>(L_1034));
		NullCheck(L_1035);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1036;
		L_1036 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1035, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1037;
		memset((&L_1037), 0, sizeof(L_1037));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1037), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1036);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1036, L_1037);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1038 = V_103;
		V_103 = ((int32_t)il2cpp_codegen_add(L_1038, 1));
	}

IL_2541:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1039 = V_103;
		int32_t L_1040 = __this->___scr_lvl_21;
		if ((((int32_t)L_1039) <= ((int32_t)L_1040)))
		{
			goto IL_250f;
		}
	}
	{
		// scr_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1041 = __this->___scr_txt_23;
		NullCheck(L_1041);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1041, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		goto IL_25ae;
	}

IL_255d:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		V_104 = 0;
		goto IL_2594;
	}

IL_2562:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1042 = __this->___lvl_scr_22;
		int32_t L_1043 = V_104;
		NullCheck(L_1042);
		int32_t L_1044 = L_1043;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1045 = (L_1042)->GetAt(static_cast<il2cpp_array_size_t>(L_1044));
		NullCheck(L_1045);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1046;
		L_1046 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1045, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1047;
		memset((&L_1047), 0, sizeof(L_1047));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1047), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1046);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1046, L_1047);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1048 = V_104;
		V_104 = ((int32_t)il2cpp_codegen_add(L_1048, 1));
	}

IL_2594:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_1049 = V_104;
		int32_t L_1050 = __this->___scr_lvl_21;
		if ((((int32_t)L_1049) <= ((int32_t)L_1050)))
		{
			goto IL_2562;
		}
	}
	{
		// scr_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1051 = __this->___scr_txt_23;
		NullCheck(L_1051);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1051, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
	}

IL_25ae:
	{
		// switch (gra_level)
		int32_t L_1052 = __this->___gra_level_18;
		V_4 = L_1052;
		int32_t L_1053 = V_4;
		switch (L_1053)
		{
			case 0:
			{
				goto IL_260e;
			}
			case 1:
			{
				goto IL_2660;
			}
			case 2:
			{
				goto IL_26b2;
			}
			case 3:
			{
				goto IL_2704;
			}
			case 4:
			{
				goto IL_2756;
			}
			case 5:
			{
				goto IL_27a8;
			}
			case 6:
			{
				goto IL_27fa;
			}
			case 7:
			{
				goto IL_284c;
			}
			case 8:
			{
				goto IL_289e;
			}
			case 9:
			{
				goto IL_28f0;
			}
			case 10:
			{
				goto IL_2942;
			}
			case 11:
			{
				goto IL_2994;
			}
			case 12:
			{
				goto IL_29e6;
			}
			case 13:
			{
				goto IL_2a38;
			}
			case 14:
			{
				goto IL_2a8a;
			}
			case 15:
			{
				goto IL_2adc;
			}
			case 16:
			{
				goto IL_2b2e;
			}
			case 17:
			{
				goto IL_2b80;
			}
			case 18:
			{
				goto IL_2bd2;
			}
			case 19:
			{
				goto IL_2c24;
			}
		}
	}
	{
		return;
	}

IL_260e:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_105 = 0;
		goto IL_2645;
	}

IL_2613:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1054 = __this->___lvl_gra_19;
		int32_t L_1055 = V_105;
		NullCheck(L_1054);
		int32_t L_1056 = L_1055;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1057 = (L_1054)->GetAt(static_cast<il2cpp_array_size_t>(L_1056));
		NullCheck(L_1057);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1058;
		L_1058 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1057, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1059;
		memset((&L_1059), 0, sizeof(L_1059));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1059), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1058);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1058, L_1059);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1060 = V_105;
		V_105 = ((int32_t)il2cpp_codegen_add(L_1060, 1));
	}

IL_2645:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1061 = V_105;
		int32_t L_1062 = __this->___gra_level_18;
		if ((((int32_t)L_1061) <= ((int32_t)L_1062)))
		{
			goto IL_2613;
		}
	}
	{
		// gra_txt.text = "$50";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1063 = __this->___gra_txt_20;
		NullCheck(L_1063);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1063, _stringLiteralC462AFA996B7598BE001D0C816359D39B959C26D);
		// break;
		return;
	}

IL_2660:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_106 = 0;
		goto IL_2697;
	}

IL_2665:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1064 = __this->___lvl_gra_19;
		int32_t L_1065 = V_106;
		NullCheck(L_1064);
		int32_t L_1066 = L_1065;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1067 = (L_1064)->GetAt(static_cast<il2cpp_array_size_t>(L_1066));
		NullCheck(L_1067);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1068;
		L_1068 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1067, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1069;
		memset((&L_1069), 0, sizeof(L_1069));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1069), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1068);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1068, L_1069);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1070 = V_106;
		V_106 = ((int32_t)il2cpp_codegen_add(L_1070, 1));
	}

IL_2697:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1071 = V_106;
		int32_t L_1072 = __this->___gra_level_18;
		if ((((int32_t)L_1071) <= ((int32_t)L_1072)))
		{
			goto IL_2665;
		}
	}
	{
		// gra_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1073 = __this->___gra_txt_20;
		NullCheck(L_1073);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1073, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		// break;
		return;
	}

IL_26b2:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_107 = 0;
		goto IL_26e9;
	}

IL_26b7:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1074 = __this->___lvl_gra_19;
		int32_t L_1075 = V_107;
		NullCheck(L_1074);
		int32_t L_1076 = L_1075;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1077 = (L_1074)->GetAt(static_cast<il2cpp_array_size_t>(L_1076));
		NullCheck(L_1077);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1078;
		L_1078 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1077, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1079;
		memset((&L_1079), 0, sizeof(L_1079));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1079), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1078);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1078, L_1079);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1080 = V_107;
		V_107 = ((int32_t)il2cpp_codegen_add(L_1080, 1));
	}

IL_26e9:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1081 = V_107;
		int32_t L_1082 = __this->___gra_level_18;
		if ((((int32_t)L_1081) <= ((int32_t)L_1082)))
		{
			goto IL_26b7;
		}
	}
	{
		// gra_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1083 = __this->___gra_txt_20;
		NullCheck(L_1083);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1083, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		// break;
		return;
	}

IL_2704:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_108 = 0;
		goto IL_273b;
	}

IL_2709:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1084 = __this->___lvl_gra_19;
		int32_t L_1085 = V_108;
		NullCheck(L_1084);
		int32_t L_1086 = L_1085;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1087 = (L_1084)->GetAt(static_cast<il2cpp_array_size_t>(L_1086));
		NullCheck(L_1087);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1088;
		L_1088 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1087, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1089;
		memset((&L_1089), 0, sizeof(L_1089));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1089), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1088);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1088, L_1089);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1090 = V_108;
		V_108 = ((int32_t)il2cpp_codegen_add(L_1090, 1));
	}

IL_273b:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1091 = V_108;
		int32_t L_1092 = __this->___gra_level_18;
		if ((((int32_t)L_1091) <= ((int32_t)L_1092)))
		{
			goto IL_2709;
		}
	}
	{
		// gra_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1093 = __this->___gra_txt_20;
		NullCheck(L_1093);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1093, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		// break;
		return;
	}

IL_2756:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_109 = 0;
		goto IL_278d;
	}

IL_275b:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1094 = __this->___lvl_gra_19;
		int32_t L_1095 = V_109;
		NullCheck(L_1094);
		int32_t L_1096 = L_1095;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1097 = (L_1094)->GetAt(static_cast<il2cpp_array_size_t>(L_1096));
		NullCheck(L_1097);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1098;
		L_1098 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1097, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1099;
		memset((&L_1099), 0, sizeof(L_1099));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1099), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1098);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1098, L_1099);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1100 = V_109;
		V_109 = ((int32_t)il2cpp_codegen_add(L_1100, 1));
	}

IL_278d:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1101 = V_109;
		int32_t L_1102 = __this->___gra_level_18;
		if ((((int32_t)L_1101) <= ((int32_t)L_1102)))
		{
			goto IL_275b;
		}
	}
	{
		// gra_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1103 = __this->___gra_txt_20;
		NullCheck(L_1103);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1103, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		// break;
		return;
	}

IL_27a8:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_110 = 0;
		goto IL_27df;
	}

IL_27ad:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1104 = __this->___lvl_gra_19;
		int32_t L_1105 = V_110;
		NullCheck(L_1104);
		int32_t L_1106 = L_1105;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1107 = (L_1104)->GetAt(static_cast<il2cpp_array_size_t>(L_1106));
		NullCheck(L_1107);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1108;
		L_1108 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1107, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1109;
		memset((&L_1109), 0, sizeof(L_1109));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1109), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1108);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1108, L_1109);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1110 = V_110;
		V_110 = ((int32_t)il2cpp_codegen_add(L_1110, 1));
	}

IL_27df:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1111 = V_110;
		int32_t L_1112 = __this->___gra_level_18;
		if ((((int32_t)L_1111) <= ((int32_t)L_1112)))
		{
			goto IL_27ad;
		}
	}
	{
		// gra_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1113 = __this->___gra_txt_20;
		NullCheck(L_1113);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1113, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		// break;
		return;
	}

IL_27fa:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_111 = 0;
		goto IL_2831;
	}

IL_27ff:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1114 = __this->___lvl_gra_19;
		int32_t L_1115 = V_111;
		NullCheck(L_1114);
		int32_t L_1116 = L_1115;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1117 = (L_1114)->GetAt(static_cast<il2cpp_array_size_t>(L_1116));
		NullCheck(L_1117);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1118;
		L_1118 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1117, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1119;
		memset((&L_1119), 0, sizeof(L_1119));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1119), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1118);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1118, L_1119);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1120 = V_111;
		V_111 = ((int32_t)il2cpp_codegen_add(L_1120, 1));
	}

IL_2831:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1121 = V_111;
		int32_t L_1122 = __this->___gra_level_18;
		if ((((int32_t)L_1121) <= ((int32_t)L_1122)))
		{
			goto IL_27ff;
		}
	}
	{
		// gra_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1123 = __this->___gra_txt_20;
		NullCheck(L_1123);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1123, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		// break;
		return;
	}

IL_284c:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_112 = 0;
		goto IL_2883;
	}

IL_2851:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1124 = __this->___lvl_gra_19;
		int32_t L_1125 = V_112;
		NullCheck(L_1124);
		int32_t L_1126 = L_1125;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1127 = (L_1124)->GetAt(static_cast<il2cpp_array_size_t>(L_1126));
		NullCheck(L_1127);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1128;
		L_1128 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1127, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1129;
		memset((&L_1129), 0, sizeof(L_1129));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1129), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1128);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1128, L_1129);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1130 = V_112;
		V_112 = ((int32_t)il2cpp_codegen_add(L_1130, 1));
	}

IL_2883:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1131 = V_112;
		int32_t L_1132 = __this->___gra_level_18;
		if ((((int32_t)L_1131) <= ((int32_t)L_1132)))
		{
			goto IL_2851;
		}
	}
	{
		// gra_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1133 = __this->___gra_txt_20;
		NullCheck(L_1133);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1133, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		// break;
		return;
	}

IL_289e:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_113 = 0;
		goto IL_28d5;
	}

IL_28a3:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1134 = __this->___lvl_gra_19;
		int32_t L_1135 = V_113;
		NullCheck(L_1134);
		int32_t L_1136 = L_1135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1137 = (L_1134)->GetAt(static_cast<il2cpp_array_size_t>(L_1136));
		NullCheck(L_1137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1138;
		L_1138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1139;
		memset((&L_1139), 0, sizeof(L_1139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1138, L_1139);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1140 = V_113;
		V_113 = ((int32_t)il2cpp_codegen_add(L_1140, 1));
	}

IL_28d5:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1141 = V_113;
		int32_t L_1142 = __this->___gra_level_18;
		if ((((int32_t)L_1141) <= ((int32_t)L_1142)))
		{
			goto IL_28a3;
		}
	}
	{
		// gra_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1143 = __this->___gra_txt_20;
		NullCheck(L_1143);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1143, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		// break;
		return;
	}

IL_28f0:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_114 = 0;
		goto IL_2927;
	}

IL_28f5:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1144 = __this->___lvl_gra_19;
		int32_t L_1145 = V_114;
		NullCheck(L_1144);
		int32_t L_1146 = L_1145;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1147 = (L_1144)->GetAt(static_cast<il2cpp_array_size_t>(L_1146));
		NullCheck(L_1147);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1148;
		L_1148 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1147, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1149;
		memset((&L_1149), 0, sizeof(L_1149));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1149), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1148);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1148, L_1149);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1150 = V_114;
		V_114 = ((int32_t)il2cpp_codegen_add(L_1150, 1));
	}

IL_2927:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1151 = V_114;
		int32_t L_1152 = __this->___gra_level_18;
		if ((((int32_t)L_1151) <= ((int32_t)L_1152)))
		{
			goto IL_28f5;
		}
	}
	{
		// gra_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1153 = __this->___gra_txt_20;
		NullCheck(L_1153);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1153, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		// break;
		return;
	}

IL_2942:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_115 = 0;
		goto IL_2979;
	}

IL_2947:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1154 = __this->___lvl_gra_19;
		int32_t L_1155 = V_115;
		NullCheck(L_1154);
		int32_t L_1156 = L_1155;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1157 = (L_1154)->GetAt(static_cast<il2cpp_array_size_t>(L_1156));
		NullCheck(L_1157);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1158;
		L_1158 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1157, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1159;
		memset((&L_1159), 0, sizeof(L_1159));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1159), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1158);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1158, L_1159);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1160 = V_115;
		V_115 = ((int32_t)il2cpp_codegen_add(L_1160, 1));
	}

IL_2979:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1161 = V_115;
		int32_t L_1162 = __this->___gra_level_18;
		if ((((int32_t)L_1161) <= ((int32_t)L_1162)))
		{
			goto IL_2947;
		}
	}
	{
		// gra_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1163 = __this->___gra_txt_20;
		NullCheck(L_1163);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1163, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		// break;
		return;
	}

IL_2994:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_116 = 0;
		goto IL_29cb;
	}

IL_2999:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1164 = __this->___lvl_gra_19;
		int32_t L_1165 = V_116;
		NullCheck(L_1164);
		int32_t L_1166 = L_1165;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1167 = (L_1164)->GetAt(static_cast<il2cpp_array_size_t>(L_1166));
		NullCheck(L_1167);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1168;
		L_1168 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1167, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1169;
		memset((&L_1169), 0, sizeof(L_1169));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1169), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1168);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1168, L_1169);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1170 = V_116;
		V_116 = ((int32_t)il2cpp_codegen_add(L_1170, 1));
	}

IL_29cb:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1171 = V_116;
		int32_t L_1172 = __this->___gra_level_18;
		if ((((int32_t)L_1171) <= ((int32_t)L_1172)))
		{
			goto IL_2999;
		}
	}
	{
		// gra_txt.text = "600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1173 = __this->___gra_txt_20;
		NullCheck(L_1173);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1173, _stringLiteral6C5B81066BA70C00A4A255E828996CB0E248D91D);
		// break;
		return;
	}

IL_29e6:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_117 = 0;
		goto IL_2a1d;
	}

IL_29eb:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1174 = __this->___lvl_gra_19;
		int32_t L_1175 = V_117;
		NullCheck(L_1174);
		int32_t L_1176 = L_1175;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1177 = (L_1174)->GetAt(static_cast<il2cpp_array_size_t>(L_1176));
		NullCheck(L_1177);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1178;
		L_1178 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1177, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1179;
		memset((&L_1179), 0, sizeof(L_1179));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1179), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1178);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1178, L_1179);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1180 = V_117;
		V_117 = ((int32_t)il2cpp_codegen_add(L_1180, 1));
	}

IL_2a1d:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1181 = V_117;
		int32_t L_1182 = __this->___gra_level_18;
		if ((((int32_t)L_1181) <= ((int32_t)L_1182)))
		{
			goto IL_29eb;
		}
	}
	{
		// gra_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1183 = __this->___gra_txt_20;
		NullCheck(L_1183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1183, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		// break;
		return;
	}

IL_2a38:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_118 = 0;
		goto IL_2a6f;
	}

IL_2a3d:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1184 = __this->___lvl_gra_19;
		int32_t L_1185 = V_118;
		NullCheck(L_1184);
		int32_t L_1186 = L_1185;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1187 = (L_1184)->GetAt(static_cast<il2cpp_array_size_t>(L_1186));
		NullCheck(L_1187);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1188;
		L_1188 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1187, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1189;
		memset((&L_1189), 0, sizeof(L_1189));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1189), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1188);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1188, L_1189);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1190 = V_118;
		V_118 = ((int32_t)il2cpp_codegen_add(L_1190, 1));
	}

IL_2a6f:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1191 = V_118;
		int32_t L_1192 = __this->___gra_level_18;
		if ((((int32_t)L_1191) <= ((int32_t)L_1192)))
		{
			goto IL_2a3d;
		}
	}
	{
		// gra_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1193 = __this->___gra_txt_20;
		NullCheck(L_1193);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1193, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		// break;
		return;
	}

IL_2a8a:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_119 = 0;
		goto IL_2ac1;
	}

IL_2a8f:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1194 = __this->___lvl_gra_19;
		int32_t L_1195 = V_119;
		NullCheck(L_1194);
		int32_t L_1196 = L_1195;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1197 = (L_1194)->GetAt(static_cast<il2cpp_array_size_t>(L_1196));
		NullCheck(L_1197);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1198;
		L_1198 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1197, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1199;
		memset((&L_1199), 0, sizeof(L_1199));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1199), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1198);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1198, L_1199);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1200 = V_119;
		V_119 = ((int32_t)il2cpp_codegen_add(L_1200, 1));
	}

IL_2ac1:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1201 = V_119;
		int32_t L_1202 = __this->___gra_level_18;
		if ((((int32_t)L_1201) <= ((int32_t)L_1202)))
		{
			goto IL_2a8f;
		}
	}
	{
		// gra_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1203 = __this->___gra_txt_20;
		NullCheck(L_1203);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1203, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		// break;
		return;
	}

IL_2adc:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_120 = 0;
		goto IL_2b13;
	}

IL_2ae1:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1204 = __this->___lvl_gra_19;
		int32_t L_1205 = V_120;
		NullCheck(L_1204);
		int32_t L_1206 = L_1205;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1207 = (L_1204)->GetAt(static_cast<il2cpp_array_size_t>(L_1206));
		NullCheck(L_1207);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1208;
		L_1208 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1207, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1209;
		memset((&L_1209), 0, sizeof(L_1209));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1209), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1208);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1208, L_1209);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1210 = V_120;
		V_120 = ((int32_t)il2cpp_codegen_add(L_1210, 1));
	}

IL_2b13:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1211 = V_120;
		int32_t L_1212 = __this->___gra_level_18;
		if ((((int32_t)L_1211) <= ((int32_t)L_1212)))
		{
			goto IL_2ae1;
		}
	}
	{
		// gra_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1213 = __this->___gra_txt_20;
		NullCheck(L_1213);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1213, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		// break;
		return;
	}

IL_2b2e:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_121 = 0;
		goto IL_2b65;
	}

IL_2b33:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1214 = __this->___lvl_gra_19;
		int32_t L_1215 = V_121;
		NullCheck(L_1214);
		int32_t L_1216 = L_1215;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1217 = (L_1214)->GetAt(static_cast<il2cpp_array_size_t>(L_1216));
		NullCheck(L_1217);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1218;
		L_1218 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1217, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1219;
		memset((&L_1219), 0, sizeof(L_1219));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1219), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1218);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1218, L_1219);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1220 = V_121;
		V_121 = ((int32_t)il2cpp_codegen_add(L_1220, 1));
	}

IL_2b65:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1221 = V_121;
		int32_t L_1222 = __this->___gra_level_18;
		if ((((int32_t)L_1221) <= ((int32_t)L_1222)))
		{
			goto IL_2b33;
		}
	}
	{
		// gra_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1223 = __this->___gra_txt_20;
		NullCheck(L_1223);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1223, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		// break;
		return;
	}

IL_2b80:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_122 = 0;
		goto IL_2bb7;
	}

IL_2b85:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1224 = __this->___lvl_gra_19;
		int32_t L_1225 = V_122;
		NullCheck(L_1224);
		int32_t L_1226 = L_1225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1227 = (L_1224)->GetAt(static_cast<il2cpp_array_size_t>(L_1226));
		NullCheck(L_1227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1228;
		L_1228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1229;
		memset((&L_1229), 0, sizeof(L_1229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1228, L_1229);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1230 = V_122;
		V_122 = ((int32_t)il2cpp_codegen_add(L_1230, 1));
	}

IL_2bb7:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1231 = V_122;
		int32_t L_1232 = __this->___gra_level_18;
		if ((((int32_t)L_1231) <= ((int32_t)L_1232)))
		{
			goto IL_2b85;
		}
	}
	{
		// gra_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1233 = __this->___gra_txt_20;
		NullCheck(L_1233);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1233, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		// break;
		return;
	}

IL_2bd2:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_123 = 0;
		goto IL_2c09;
	}

IL_2bd7:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1234 = __this->___lvl_gra_19;
		int32_t L_1235 = V_123;
		NullCheck(L_1234);
		int32_t L_1236 = L_1235;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1237 = (L_1234)->GetAt(static_cast<il2cpp_array_size_t>(L_1236));
		NullCheck(L_1237);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1238;
		L_1238 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1237, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1239;
		memset((&L_1239), 0, sizeof(L_1239));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1239), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1238);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1238, L_1239);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1240 = V_123;
		V_123 = ((int32_t)il2cpp_codegen_add(L_1240, 1));
	}

IL_2c09:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1241 = V_123;
		int32_t L_1242 = __this->___gra_level_18;
		if ((((int32_t)L_1241) <= ((int32_t)L_1242)))
		{
			goto IL_2bd7;
		}
	}
	{
		// gra_txt.text = "$950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1243 = __this->___gra_txt_20;
		NullCheck(L_1243);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1243, _stringLiteral46D3D7990C24451C5D06EE471F66F5BFA6363F43);
		// break;
		return;
	}

IL_2c24:
	{
		// for (int i = 0; i <= gra_level; i++)
		V_124 = 0;
		goto IL_2c5b;
	}

IL_2c29:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1244 = __this->___lvl_gra_19;
		int32_t L_1245 = V_124;
		NullCheck(L_1244);
		int32_t L_1246 = L_1245;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1247 = (L_1244)->GetAt(static_cast<il2cpp_array_size_t>(L_1246));
		NullCheck(L_1247);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_1248;
		L_1248 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_1247, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1249;
		memset((&L_1249), 0, sizeof(L_1249));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_1249), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_1248);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1248, L_1249);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1250 = V_124;
		V_124 = ((int32_t)il2cpp_codegen_add(L_1250, 1));
	}

IL_2c5b:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_1251 = V_124;
		int32_t L_1252 = __this->___gra_level_18;
		if ((((int32_t)L_1251) <= ((int32_t)L_1252)))
		{
			goto IL_2c29;
		}
	}
	{
		// gra_txt.text = "$1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1253 = __this->___gra_txt_20;
		NullCheck(L_1253);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1253, _stringLiteralF48EC6378FFB285C34651B486A97A562CE16D3F2);
		// }
		return;
	}
}
// System.Void up_manager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_Update_mEB3B61E0A2507F98A9E278414AA21A67CD77A452 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float money = PlayerPrefs.GetFloat("money");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_0 = L_0;
		// money_txt.text =  money.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1 = __this->___money_txt_6;
		String_t* L_2;
		L_2 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972((&V_0), NULL);
		NullCheck(L_1);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		// float gems = PlayerPrefs.GetFloat("gem");
		float L_3;
		L_3 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, NULL);
		V_1 = L_3;
		// gems_txt.text =  gems.ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_4 = __this->___gems_txt_7;
		String_t* L_5;
		L_5 = Single_ToString_mE282EDA9CA4F7DF88432D807732837A629D04972((&V_1), NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		// }
		return;
	}
}
// System.Void up_manager::multiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_multiplier_mEA5BBF6799CE234D9671BBE54AAF279A8B5C1A83 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("mul_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("mul_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 1f;
		V_0 = (1.0f);
	}

IL_0028:
	{
		// switch (level)
		int32_t L_3 = __this->___level_8;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 0.25f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (0.25f)));
		// level = level +1;
		int32_t L_7 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= level; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_mul_4;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= level; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___level_8;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_17, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_18 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// mul_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___mul_txt_5;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 0.25f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (0.25f)));
		// level = level + 1;
		int32_t L_25 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= level; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_mul_4;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= level; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___level_8;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_35, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_36 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// mul_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___mul_txt_5;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 0.25f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (0.25f)));
		// level = level + 1;
		int32_t L_43 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= level; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_mul_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___level_8;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_53, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_54 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// mul_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___mul_txt_5;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 0.25f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (0.25f)));
		// level = level + 1;
		int32_t L_61 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_mul_4;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___level_8;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_71, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_72 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// mul_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___mul_txt_5;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 0.25f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (0.25f)));
		// level = level + 1;
		int32_t L_79 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_mul_4;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= level; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___level_8;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_89, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_90 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// mul_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___mul_txt_5;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 0.25f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (0.25f)));
		// level = level + 1;
		int32_t L_97 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= level; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_mul_4;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= level; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___level_8;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_107, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_108 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// mul_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___mul_txt_5;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 0.25f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (0.25f)));
		// level = level + 1;
		int32_t L_115 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= level; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_mul_4;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= level; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___level_8;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_125, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_126 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// mul_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___mul_txt_5;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 0.25f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (0.25f)));
		// level = level + 1;
		int32_t L_133 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= level; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_mul_4;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= level; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___level_8;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_143, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_144 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// mul_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___mul_txt_5;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 0.25f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (0.25f)));
		// level = level + 1;
		int32_t L_151 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= level; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_mul_4;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= level; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___level_8;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_161, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_162 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// mul_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___mul_txt_5;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 0.25f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (0.25f)));
		// level = level + 1;
		int32_t L_169 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= level; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_mul_4;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= level; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___level_8;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_179, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_180 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// mul_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___mul_txt_5;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 0.25f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (0.25f)));
		// level = level + 1;
		int32_t L_187 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= level; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_mul_4;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= level; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___level_8;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_197, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_198 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// mul_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___mul_txt_5;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 0.25f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (0.25f)));
		// level = level + 1;
		int32_t L_205 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= level; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_mul_4;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= level; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___level_8;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_215, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_216 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// mul_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___mul_txt_5;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 0.25f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (0.25f)));
		// level = level + 1;
		int32_t L_223 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= level; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_mul_4;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= level; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___level_8;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_233, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_234 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// mul_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___mul_txt_5;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 0.25f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (0.25f)));
		// level = level + 1;
		int32_t L_241 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= level; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_mul_4;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= level; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___level_8;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_251, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_252 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// mul_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___mul_txt_5;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 0.25f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (0.25f)));
		// level = level + 1;
		int32_t L_259 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= level; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_mul_4;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= level; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___level_8;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_269, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_270 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// mul_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___mul_txt_5;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 0.25f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (0.25f)));
		// level = level + 1;
		int32_t L_277 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= level; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_mul_4;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= level; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___level_8;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_287, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_288 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// mul_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___mul_txt_5;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 0.25f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (0.25f)));
		// level = level + 1;
		int32_t L_295 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= level; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_mul_4;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= level; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___level_8;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_305, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_306 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// mul_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___mul_txt_5;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 0.25f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (0.25f)));
		// level = level + 1;
		int32_t L_313 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= level; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_mul_4;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= level; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___level_8;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_323, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_324 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// mul_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___mul_txt_5;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 0.25f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (0.25f)));
		// level = level + 1;
		int32_t L_331 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= level; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_mul_4;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= level; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___level_8;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_341, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_342 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// mul_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___mul_txt_5;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 0.25f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (0.25f)));
		// level = level + 1;
		int32_t L_349 = __this->___level_8;
		__this->___level_8 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= level; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_mul[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_mul_4;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= level; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= level; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___level_8;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("mul_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral592FE1AFAC6A8B856442C3318F87308B1CC2E304, L_359, NULL);
		// PlayerPrefs.SetInt("pow_lvl", level);
		int32_t L_360 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral9DE4F2A7E4EBCAABECF2CCC356197A08FB550C52, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// mul_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___mul_txt_5;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::return_home()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_return_home_mC453F694A23A881D5699F3B05F4716484883A37C (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(0);
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m0957E62F2A0A0243C79394E5B74E8EFA86BE5ED1(0, NULL);
		// }
		return;
	}
}
// System.Void up_manager::resistancier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_resistancier_m1D6ABD9F44C6B181B7878A0D2D788EF7A8F397C1 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("res_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("res_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 5f;
		V_0 = (5.0f);
	}

IL_0028:
	{
		// switch (level)
		int32_t L_3 = __this->___level_8;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 0.25f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_7 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= res_level; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_resis_10;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___res_level_9;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_17, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_18 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// res_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___res_txt_11;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 0.25f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_25 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= res_level; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_resis_10;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___res_level_9;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_35, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_36 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// res_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___res_txt_11;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 0.25f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_43 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= res_level; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_resis_10;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___res_level_9;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_53, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_54 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// res_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___res_txt_11;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 0.25f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_61 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= res_level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_resis_10;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___res_level_9;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_71, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_72 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// res_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___res_txt_11;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 0.25f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_79 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= res_level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_resis_10;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___res_level_9;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_89, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_90 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// res_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___res_txt_11;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 0.25f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_97 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= res_level; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_resis_10;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___res_level_9;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_107, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_108 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// res_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___res_txt_11;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 0.25f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_115 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= res_level; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_resis_10;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___res_level_9;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_125, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_126 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// res_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___res_txt_11;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 0.25f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_133 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= res_level; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_resis_10;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___res_level_9;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_143, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_144 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// res_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___res_txt_11;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 0.25f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_151 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= res_level; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_resis_10;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___res_level_9;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_161, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_162 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// res_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___res_txt_11;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 0.25f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_169 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= res_level; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_resis_10;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___res_level_9;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_179, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_180 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// res_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___res_txt_11;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 0.25f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_187 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= res_level; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_resis_10;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___res_level_9;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_197, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_198 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// res_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___res_txt_11;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 0.25f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_205 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= res_level; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_resis_10;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___res_level_9;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_215, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_216 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// res_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___res_txt_11;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 0.25f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_223 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= res_level; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_resis_10;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___res_level_9;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_233, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_234 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// res_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___res_txt_11;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 0.25f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_241 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= res_level; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_resis_10;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___res_level_9;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_251, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_252 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// res_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___res_txt_11;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 0.25f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_259 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= res_level; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_resis_10;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___res_level_9;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_269, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_270 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// res_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___res_txt_11;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 0.25f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_277 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= res_level; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_resis_10;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___res_level_9;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_287, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_288 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// res_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___res_txt_11;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 0.25f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_295 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= res_level; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_resis_10;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___res_level_9;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_305, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_306 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// res_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___res_txt_11;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 0.25f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_313 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= res_level; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_resis_10;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___res_level_9;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_323, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_324 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// res_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___res_txt_11;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 0.25f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_331 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= res_level; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_resis_10;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___res_level_9;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_341, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_342 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// res_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___res_txt_11;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 0.25f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (0.25f)));
		// res_level = res_level + 1;
		int32_t L_349 = __this->___res_level_9;
		__this->___res_level_9 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= res_level; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_resis[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_resis_10;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= res_level; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= res_level; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___res_level_9;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("res_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD27175A6522D477A6F4AA32B4E568599DD6264DA, L_359, NULL);
		// PlayerPrefs.SetInt("res_lvl", level);
		int32_t L_360 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral0709BFBBD0134A81816B1EAE1613685936B34B94, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// res_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___res_txt_11;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::coinifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_coinifier_m553B5F1C52E66F92E5D845C0DD535B7C498DF1D0 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("coin_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("coin_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 50f;
		V_0 = (50.0f);
	}

IL_0028:
	{
		// switch (coin_level)
		int32_t L_3 = __this->___coin_level_12;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 5f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_7 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_coin_13;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___coin_level_12;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_17, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_18 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// coin_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___coin_txt_14;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 5f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_25 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_coin_13;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___coin_level_12;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_35, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_36 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// res_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___res_txt_11;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 5f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_43 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_coin_13;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___coin_level_12;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_53, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_54 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// coin_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___coin_txt_14;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 5f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_61 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_coin_13;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___coin_level_12;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_71, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_72 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// coin_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___coin_txt_14;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 5f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_79 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_coin_13;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___coin_level_12;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_89, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_90 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// coin_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___coin_txt_14;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 5f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_97 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_coin_13;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___coin_level_12;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_107, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_108 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// coin_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___coin_txt_14;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 5f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_115 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_coin_13;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___coin_level_12;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_125, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_126 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// coin_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___coin_txt_14;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 5f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_133 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_coin_13;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___coin_level_12;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_143, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_144 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// coin_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___coin_txt_14;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 5f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_151 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_coin_13;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___coin_level_12;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_161, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_162 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// coin_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___coin_txt_14;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 5f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_169 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_coin_13;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___coin_level_12;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_179, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_180 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// coin_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___coin_txt_14;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 5f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_187 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_coin_13;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___coin_level_12;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_197, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_198 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// coin_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___coin_txt_14;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 5f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_205 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_coin_13;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___coin_level_12;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_215, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_216 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// coin_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___coin_txt_14;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 5f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_223 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_coin_13;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___coin_level_12;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_233, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_234 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// coin_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___coin_txt_14;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 5f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_241 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_coin_13;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___coin_level_12;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_251, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_252 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// coin_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___coin_txt_14;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 5f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_259 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_coin_13;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___coin_level_12;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_269, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_270 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// coin_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___coin_txt_14;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 5f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_277 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_coin_13;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___coin_level_12;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_287, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_288 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// coin_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___coin_txt_14;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 5f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_295 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_coin_13;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___coin_level_12;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_305, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_306 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// coin_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___coin_txt_14;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 5f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_313 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_coin_13;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___coin_level_12;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_323, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_324 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// coin_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___coin_txt_14;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 5f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_331 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_coin_13;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___coin_level_12;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_341, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_342 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// coin_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___coin_txt_14;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 5f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (5.0f)));
		// coin_level = coin_level + 1;
		int32_t L_349 = __this->___coin_level_12;
		__this->___coin_level_12 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= coin_level; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_coin[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_coin_13;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= coin_level; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___coin_level_12;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("coin_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralA47F62393E03A1730DF800BCE6D620FE4EFFE2BA, L_359, NULL);
		// PlayerPrefs.SetInt("coin_lvl", coin_level);
		int32_t L_360 = __this->___coin_level_12;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral265BD5A3837C2FA731DBE75C2F11955160FD9355, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// coin_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___coin_txt_14;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::increase_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_increase_velocity_m82A834E56BBD4FA0E094B3221D0A6F17B3771DA3 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("vel_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("vel_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 8f;
		V_0 = (8.0f);
	}

IL_0028:
	{
		// switch (vel_level)
		int32_t L_3 = __this->___vel_level_15;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 0.3f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_7 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_vel_16;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___vel_level_15;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_17, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_18 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// vel_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___vel_txt_17;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 0.3f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_25 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_vel_16;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___vel_level_15;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_35, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_36 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// vel_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___vel_txt_17;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 0.3f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_43 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_vel_16;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___vel_level_15;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_53, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_54 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// vel_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___vel_txt_17;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 0.3f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_61 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_vel_16;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___vel_level_15;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_71, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_72 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// vel_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___vel_txt_17;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 0.3f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_79 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_vel_16;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___vel_level_15;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_89, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_90 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// vel_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___vel_txt_17;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 0.3f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_97 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_vel_16;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___vel_level_15;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_107, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_108 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// vel_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___vel_txt_17;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 0.3f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_115 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_vel_16;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___vel_level_15;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_125, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_126 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// vel_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___vel_txt_17;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 0.3f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_133 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_vel_16;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___vel_level_15;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_143, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_144 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// vel_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___vel_txt_17;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 0.3f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_151 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_vel_16;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___vel_level_15;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_161, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_162 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// vel_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___vel_txt_17;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 0.3f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_169 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_vel_16;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___vel_level_15;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_179, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_180 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// vel_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___vel_txt_17;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 0.3f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_187 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_vel_16;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___vel_level_15;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_197, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_198 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// vel_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___vel_txt_17;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 0.3f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_205 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_vel_16;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___vel_level_15;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_215, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_216 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// vel_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___vel_txt_17;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 0.3f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_223 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_vel_16;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___vel_level_15;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_233, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_234 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// vel_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___vel_txt_17;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 0.3f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_241 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_vel_16;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___vel_level_15;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_251, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_252 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// vel_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___vel_txt_17;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 0.3f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_259 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_vel_16;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___vel_level_15;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_269, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_270 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// vel_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___vel_txt_17;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 0.3f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_277 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_vel_16;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___vel_level_15;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_287, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_288 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// vel_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___vel_txt_17;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 0.3f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_295 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_vel_16;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___vel_level_15;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_305, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_306 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// vel_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___vel_txt_17;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 0.3f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_313 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_vel_16;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___vel_level_15;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_323, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_324 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// vel_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___vel_txt_17;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 0.3f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_331 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_vel_16;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___vel_level_15;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_341, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_342 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// vel_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___vel_txt_17;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 0.3f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (0.300000012f)));
		// vel_level = vel_level + 1;
		int32_t L_349 = __this->___vel_level_15;
		__this->___vel_level_15 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= vel_level; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_vel[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_vel_16;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= vel_level; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___vel_level_15;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("vel_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral57698CF9EE3E01EE3A790E0D40A8183C35158957, L_359, NULL);
		// PlayerPrefs.SetInt("vel_lvl", vel_level);
		int32_t L_360 = __this->___vel_level_15;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral13B15B51B49EB5638EFEC5DB4913A0836807E986, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// vel_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___vel_txt_17;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::decrease_fall()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_decrease_fall_mFC091DD4D51110A5BAE212F278733DEBB438CBB0 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("gra_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("gra_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 0f;
		V_0 = (0.0f);
	}

IL_0028:
	{
		// switch (gra_level)
		int32_t L_3 = __this->___gra_level_18;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 0.05f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_7 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_gra_19;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___gra_level_18;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_17, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_18 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// gra_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___gra_txt_20;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 0.05f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_25 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_gra_19;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___gra_level_18;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_35, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_36 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// gra_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___gra_txt_20;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 0.05f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_43 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_gra_19;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___gra_level_18;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_53, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_54 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// gra_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___gra_txt_20;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 0.05f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_61 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_gra_19;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___gra_level_18;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_71, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_72 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// gra_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___gra_txt_20;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 0.05f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_79 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_gra_19;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___gra_level_18;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_89, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_90 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// gra_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___gra_txt_20;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 0.05f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_97 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_gra_19;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___gra_level_18;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_107, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_108 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// gra_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___gra_txt_20;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 0.05f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_115 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_gra_19;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___gra_level_18;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_125, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_126 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// gra_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___gra_txt_20;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 0.05f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_133 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_gra_19;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___gra_level_18;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_143, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_144 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// gra_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___gra_txt_20;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 0.05f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_151 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_gra_19;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___gra_level_18;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_161, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_162 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// gra_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___gra_txt_20;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 0.05f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_169 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_gra_19;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___gra_level_18;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_179, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_180 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// gra_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___gra_txt_20;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 0.05f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_187 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_gra_19;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___gra_level_18;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_197, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_198 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// gra_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___gra_txt_20;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 0.05f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_205 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_gra_19;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___gra_level_18;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_215, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_216 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// gra_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___gra_txt_20;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 0.05f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_223 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_gra_19;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___gra_level_18;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_233, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_234 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// gra_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___gra_txt_20;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 0.05f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_241 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_gra_19;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___gra_level_18;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_251, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_252 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// gra_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___gra_txt_20;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 0.05f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_259 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_gra_19;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___gra_level_18;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_269, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_270 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// gra_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___gra_txt_20;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 0.05f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_277 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_gra_19;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___gra_level_18;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_287, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_288 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// gra_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___gra_txt_20;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 0.05f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_295 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_gra_19;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___gra_level_18;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_305, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_306 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// gra_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___gra_txt_20;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 0.05f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_313 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_gra_19;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___gra_level_18;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_323, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_324 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// gra_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___gra_txt_20;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 0.05f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_331 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_gra_19;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___gra_level_18;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_341, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_342 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// gra_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___gra_txt_20;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 0.05f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (0.0500000007f)));
		// gra_level = gra_level + 1;
		int32_t L_349 = __this->___gra_level_18;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_gra_19;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___gra_level_18;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_359, NULL);
		// PlayerPrefs.SetInt("gra_lvl", gra_level);
		int32_t L_360 = __this->___gra_level_18;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// gra_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___gra_txt_20;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::increase_score_fill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_increase_score_fill_m77F311570C815A987346E148F18B24F307D36B5E (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	{
		// float power = PlayerPrefs.GetFloat("scr_pow");
		float L_0;
		L_0 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, NULL);
		V_0 = L_0;
		// float money = PlayerPrefs.GetFloat("money");
		float L_1;
		L_1 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, NULL);
		V_1 = L_1;
		// if (!PlayerPrefs.HasKey("scr_pow"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// power = 1f;
		V_0 = (1.0f);
	}

IL_0028:
	{
		// switch (scr_lvl)
		int32_t L_3 = __this->___scr_lvl_21;
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_012f;
			}
			case 2:
			{
				goto IL_01dd;
			}
			case 3:
			{
				goto IL_028b;
			}
			case 4:
			{
				goto IL_0339;
			}
			case 5:
			{
				goto IL_03e7;
			}
			case 6:
			{
				goto IL_0495;
			}
			case 7:
			{
				goto IL_0543;
			}
			case 8:
			{
				goto IL_05f1;
			}
			case 9:
			{
				goto IL_069f;
			}
			case 10:
			{
				goto IL_074d;
			}
			case 11:
			{
				goto IL_07fb;
			}
			case 12:
			{
				goto IL_08a9;
			}
			case 13:
			{
				goto IL_0957;
			}
			case 14:
			{
				goto IL_0a05;
			}
			case 15:
			{
				goto IL_0ab3;
			}
			case 16:
			{
				goto IL_0b61;
			}
			case 17:
			{
				goto IL_0c0f;
			}
			case 18:
			{
				goto IL_0cbd;
			}
			case 19:
			{
				goto IL_0d6b;
			}
		}
	}
	{
		return;
	}

IL_0086:
	{
		// if (money >= 50)
		float L_5 = V_1;
		if ((!(((float)L_5) >= ((float)(50.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// power = power + 0.25f;
		float L_6 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_6, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_7 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_3 = 0;
		goto IL_00da;
	}

IL_00ab:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_8 = __this->___lvl_scr_22;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_12;
		L_12 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_11, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_13), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_00da:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = __this->___scr_lvl_21;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_00ab;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_17 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_17, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_18 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_18, NULL);
		// money = money - 50;
		float L_19 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_19, (50.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_20 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_20, NULL);
		// scr_txt.text = "$100";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_21 = __this->___scr_txt_23;
		NullCheck(L_21);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, _stringLiteral366752BDF8A1F128AD72833E58F62F83451C08F2);
		return;
	}

IL_0122:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___warningpanal_33;
		NullCheck(L_22);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_22, (bool)1, NULL);
		// break;
		return;
	}

IL_012f:
	{
		// if (money >= 100)
		float L_23 = V_1;
		if ((!(((float)L_23) >= ((float)(100.0f)))))
		{
			goto IL_01d0;
		}
	}
	{
		// power = power + 0.25f;
		float L_24 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_24, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_25 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_4 = 0;
		goto IL_0187;
	}

IL_0155:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_26 = __this->___lvl_scr_22;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_30;
		L_30 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_29, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_31;
		memset((&L_31), 0, sizeof(L_31));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_31), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0187:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = __this->___scr_lvl_21;
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_0155;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_35 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_35, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_36 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_36, NULL);
		// money = money - 100;
		float L_37 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_37, (100.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_38 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_38, NULL);
		// scr_txt.text = "$150";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_39 = __this->___scr_txt_23;
		NullCheck(L_39);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, _stringLiteralB3B671948C0346165D7D088112D5F275E42311AE);
		return;
	}

IL_01d0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___warningpanal_33;
		NullCheck(L_40);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_40, (bool)1, NULL);
		// break;
		return;
	}

IL_01dd:
	{
		// if (money >= 150)
		float L_41 = V_1;
		if ((!(((float)L_41) >= ((float)(150.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		// power = power + 0.25f;
		float L_42 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_42, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_43 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_43, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_5 = 0;
		goto IL_0235;
	}

IL_0203:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = __this->___lvl_scr_22;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_47);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_48;
		L_48 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_47, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_49), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_48, L_49);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_50 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_50, 1));
	}

IL_0235:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_51 = V_5;
		int32_t L_52 = __this->___scr_lvl_21;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_0203;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_53 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_53, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_54 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_54, NULL);
		// money = money - 150;
		float L_55 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_55, (150.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_56 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_56, NULL);
		// scr_txt.text = "$200";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_57 = __this->___scr_txt_23;
		NullCheck(L_57);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteralB48CDAB081178B8CC177D4CD47C093C7B54D85C7);
		return;
	}

IL_027e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_58 = __this->___warningpanal_33;
		NullCheck(L_58);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_58, (bool)1, NULL);
		// break;
		return;
	}

IL_028b:
	{
		// if (money >= 200)
		float L_59 = V_1;
		if ((!(((float)L_59) >= ((float)(200.0f)))))
		{
			goto IL_032c;
		}
	}
	{
		// power = power + 0.25f;
		float L_60 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_60, (0.25f)));
		// gra_level = vel_level + 1;
		int32_t L_61 = __this->___vel_level_15;
		__this->___gra_level_18 = ((int32_t)il2cpp_codegen_add(L_61, 1));
		// for (int i = 0; i <= gra_level; i++)
		V_6 = 0;
		goto IL_02e3;
	}

IL_02b1:
	{
		// lvl_gra[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_62 = __this->___lvl_gra_19;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_66;
		L_66 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_65, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_67;
		memset((&L_67), 0, sizeof(L_67));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_67), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_66, L_67);
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_68 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_02e3:
	{
		// for (int i = 0; i <= gra_level; i++)
		int32_t L_69 = V_6;
		int32_t L_70 = __this->___gra_level_18;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_02b1;
		}
	}
	{
		// PlayerPrefs.SetFloat("gra_pow", power);
		float L_71 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralE7A63BD79082B99D4F12E98CD4965D940EE30ED5, L_71, NULL);
		// PlayerPrefs.SetInt("gra_lvl", level);
		int32_t L_72 = __this->___level_8;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral6CFE52F83CEDFD1C66C2E662946643A6108F2F8C, L_72, NULL);
		// money = money - 200;
		float L_73 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_73, (200.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_74 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_74, NULL);
		// scr_txt.text = "$250";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_75 = __this->___scr_txt_23;
		NullCheck(L_75);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral9C4136D521F4A00170BBD2EFF06ADA20315562CC);
		return;
	}

IL_032c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_76 = __this->___warningpanal_33;
		NullCheck(L_76);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_76, (bool)1, NULL);
		// break;
		return;
	}

IL_0339:
	{
		// if (money >= 250)
		float L_77 = V_1;
		if ((!(((float)L_77) >= ((float)(250.0f)))))
		{
			goto IL_03da;
		}
	}
	{
		// power = power + 0.25f;
		float L_78 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_78, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_79 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_79, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_7 = 0;
		goto IL_0391;
	}

IL_035f:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_80 = __this->___lvl_scr_22;
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_84;
		L_84 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_83, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_85;
		memset((&L_85), 0, sizeof(L_85));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_85), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_84, L_85);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_86 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_86, 1));
	}

IL_0391:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_87 = V_7;
		int32_t L_88 = __this->___scr_lvl_21;
		if ((((int32_t)L_87) <= ((int32_t)L_88)))
		{
			goto IL_035f;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_89 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_89, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_90 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_90, NULL);
		// money = money - 250;
		float L_91 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_91, (250.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_92 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_92, NULL);
		// scr_txt.text = "$300";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_93 = __this->___scr_txt_23;
		NullCheck(L_93);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_93, _stringLiteral8851F3C45A038262A9ADAD9C27A2754A99F37470);
		return;
	}

IL_03da:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = __this->___warningpanal_33;
		NullCheck(L_94);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_94, (bool)1, NULL);
		// break;
		return;
	}

IL_03e7:
	{
		// if (money >= 300)
		float L_95 = V_1;
		if ((!(((float)L_95) >= ((float)(300.0f)))))
		{
			goto IL_0488;
		}
	}
	{
		// power = power + 0.25f;
		float L_96 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_96, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_97 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_97, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_8 = 0;
		goto IL_043f;
	}

IL_040d:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_98 = __this->___lvl_scr_22;
		int32_t L_99 = V_8;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_102;
		L_102 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_101, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_102);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_102, L_103);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_104 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_104, 1));
	}

IL_043f:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_105 = V_8;
		int32_t L_106 = __this->___scr_lvl_21;
		if ((((int32_t)L_105) <= ((int32_t)L_106)))
		{
			goto IL_040d;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_107 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_107, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_108 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_108, NULL);
		// money = money - 300;
		float L_109 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_109, (300.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_110 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_110, NULL);
		// scr_txt.text = "$350";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_111 = __this->___scr_txt_23;
		NullCheck(L_111);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_111, _stringLiteral43C46879D4C5937966348B41F9100252F6E5F88F);
		return;
	}

IL_0488:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_112 = __this->___warningpanal_33;
		NullCheck(L_112);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_112, (bool)1, NULL);
		// break;
		return;
	}

IL_0495:
	{
		// if (money >= 350)
		float L_113 = V_1;
		if ((!(((float)L_113) >= ((float)(350.0f)))))
		{
			goto IL_0536;
		}
	}
	{
		// power = power + 0.25f;
		float L_114 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_114, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_115 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_115, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_9 = 0;
		goto IL_04ed;
	}

IL_04bb:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_116 = __this->___lvl_scr_22;
		int32_t L_117 = V_9;
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		NullCheck(L_119);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_120;
		L_120 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_119, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_121;
		memset((&L_121), 0, sizeof(L_121));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_121), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_120, L_121);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_122 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add(L_122, 1));
	}

IL_04ed:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_123 = V_9;
		int32_t L_124 = __this->___scr_lvl_21;
		if ((((int32_t)L_123) <= ((int32_t)L_124)))
		{
			goto IL_04bb;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_125 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_125, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_126 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_126, NULL);
		// money = money - 350;
		float L_127 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_127, (350.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_128 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_128, NULL);
		// scr_txt.text = "$400";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_129 = __this->___scr_txt_23;
		NullCheck(L_129);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, _stringLiteralE147BCC6CB745C79E6ACF7F154E5FB28AD01A2F4);
		return;
	}

IL_0536:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_130 = __this->___warningpanal_33;
		NullCheck(L_130);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_130, (bool)1, NULL);
		// break;
		return;
	}

IL_0543:
	{
		// if (money >= 400)
		float L_131 = V_1;
		if ((!(((float)L_131) >= ((float)(400.0f)))))
		{
			goto IL_05e4;
		}
	}
	{
		// power = power + 0.25f;
		float L_132 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_132, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_133 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_133, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_10 = 0;
		goto IL_059b;
	}

IL_0569:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_134 = __this->___lvl_scr_22;
		int32_t L_135 = V_10;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		NullCheck(L_137);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_138;
		L_138 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_137, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_139;
		memset((&L_139), 0, sizeof(L_139));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_139), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_138, L_139);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_140 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add(L_140, 1));
	}

IL_059b:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_141 = V_10;
		int32_t L_142 = __this->___scr_lvl_21;
		if ((((int32_t)L_141) <= ((int32_t)L_142)))
		{
			goto IL_0569;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_143 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_143, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_144 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_144, NULL);
		// money = money - 400;
		float L_145 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_145, (400.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_146 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_146, NULL);
		// scr_txt.text = "$450";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_147 = __this->___scr_txt_23;
		NullCheck(L_147);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_147, _stringLiteral4B55C06EEC325A7FDDFC896233186811B771A8BC);
		return;
	}

IL_05e4:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_148 = __this->___warningpanal_33;
		NullCheck(L_148);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_148, (bool)1, NULL);
		// break;
		return;
	}

IL_05f1:
	{
		// if (money >= 450)
		float L_149 = V_1;
		if ((!(((float)L_149) >= ((float)(450.0f)))))
		{
			goto IL_0692;
		}
	}
	{
		// power = power + 0.25f;
		float L_150 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_150, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_151 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_151, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_11 = 0;
		goto IL_0649;
	}

IL_0617:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_152 = __this->___lvl_scr_22;
		int32_t L_153 = V_11;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_156;
		L_156 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_155, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_157;
		memset((&L_157), 0, sizeof(L_157));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_157), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_156, L_157);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_158 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add(L_158, 1));
	}

IL_0649:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_159 = V_11;
		int32_t L_160 = __this->___scr_lvl_21;
		if ((((int32_t)L_159) <= ((int32_t)L_160)))
		{
			goto IL_0617;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_161 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_161, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_162 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_162, NULL);
		// money = money - 450;
		float L_163 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_163, (450.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_164 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_164, NULL);
		// scr_txt.text = "$500";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_165 = __this->___scr_txt_23;
		NullCheck(L_165);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_165, _stringLiteral59BBDE3B3587AE763CEFA6F2807655A9A1667D39);
		return;
	}

IL_0692:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_166 = __this->___warningpanal_33;
		NullCheck(L_166);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_166, (bool)1, NULL);
		// break;
		return;
	}

IL_069f:
	{
		// if (money >= 500)
		float L_167 = V_1;
		if ((!(((float)L_167) >= ((float)(500.0f)))))
		{
			goto IL_0740;
		}
	}
	{
		// power = power + 0.25f;
		float L_168 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_168, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_169 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_169, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_12 = 0;
		goto IL_06f7;
	}

IL_06c5:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_170 = __this->___lvl_scr_22;
		int32_t L_171 = V_12;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_174;
		L_174 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_173, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_175), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_174);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_174, L_175);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_176 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_176, 1));
	}

IL_06f7:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_177 = V_12;
		int32_t L_178 = __this->___scr_lvl_21;
		if ((((int32_t)L_177) <= ((int32_t)L_178)))
		{
			goto IL_06c5;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_179 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_179, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_180 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_180, NULL);
		// money = money - 500;
		float L_181 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_181, (500.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_182 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_182, NULL);
		// scr_txt.text = "$550";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_183 = __this->___scr_txt_23;
		NullCheck(L_183);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_183, _stringLiteral0EC57BE6AF1C23F12C549E73006BAE233D9855F1);
		return;
	}

IL_0740:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_184 = __this->___warningpanal_33;
		NullCheck(L_184);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_184, (bool)1, NULL);
		// break;
		return;
	}

IL_074d:
	{
		// if (money >= 550)
		float L_185 = V_1;
		if ((!(((float)L_185) >= ((float)(550.0f)))))
		{
			goto IL_07ee;
		}
	}
	{
		// power = power + 0.25f;
		float L_186 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_186, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_187 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_187, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_13 = 0;
		goto IL_07a5;
	}

IL_0773:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_188 = __this->___lvl_scr_22;
		int32_t L_189 = V_13;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_192;
		L_192 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_191, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_193;
		memset((&L_193), 0, sizeof(L_193));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_193), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_192);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_192, L_193);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_194 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add(L_194, 1));
	}

IL_07a5:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_195 = V_13;
		int32_t L_196 = __this->___scr_lvl_21;
		if ((((int32_t)L_195) <= ((int32_t)L_196)))
		{
			goto IL_0773;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_197 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_197, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_198 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_198, NULL);
		// money = money - 550;
		float L_199 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_199, (550.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_200 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_200, NULL);
		// scr_txt.text = "$600";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_201 = __this->___scr_txt_23;
		NullCheck(L_201);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_201, _stringLiteral7988BAEC077C5A497672336B4A5DFE7DC429BC31);
		return;
	}

IL_07ee:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_202 = __this->___warningpanal_33;
		NullCheck(L_202);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_202, (bool)1, NULL);
		// break;
		return;
	}

IL_07fb:
	{
		// if (money >= 600)
		float L_203 = V_1;
		if ((!(((float)L_203) >= ((float)(600.0f)))))
		{
			goto IL_089c;
		}
	}
	{
		// power = power + 0.25f;
		float L_204 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_204, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_205 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_205, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_14 = 0;
		goto IL_0853;
	}

IL_0821:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_206 = __this->___lvl_scr_22;
		int32_t L_207 = V_14;
		NullCheck(L_206);
		int32_t L_208 = L_207;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_209 = (L_206)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		NullCheck(L_209);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_210;
		L_210 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_209, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_211;
		memset((&L_211), 0, sizeof(L_211));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_211), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_210);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_210, L_211);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_212 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_212, 1));
	}

IL_0853:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_213 = V_14;
		int32_t L_214 = __this->___scr_lvl_21;
		if ((((int32_t)L_213) <= ((int32_t)L_214)))
		{
			goto IL_0821;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_215 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_215, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_216 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_216, NULL);
		// money = money - 600;
		float L_217 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_217, (600.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_218 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_218, NULL);
		// scr_txt.text = "$650";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_219 = __this->___scr_txt_23;
		NullCheck(L_219);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_219, _stringLiteral6698F5079C31EEC13FCD18BC3091CB1C9873A7AF);
		return;
	}

IL_089c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = __this->___warningpanal_33;
		NullCheck(L_220);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_220, (bool)1, NULL);
		// break;
		return;
	}

IL_08a9:
	{
		// if (money >= 650)
		float L_221 = V_1;
		if ((!(((float)L_221) >= ((float)(650.0f)))))
		{
			goto IL_094a;
		}
	}
	{
		// power = power + 0.25f;
		float L_222 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_222, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_223 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_223, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_15 = 0;
		goto IL_0901;
	}

IL_08cf:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_224 = __this->___lvl_scr_22;
		int32_t L_225 = V_15;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_228;
		L_228 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_227, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_229;
		memset((&L_229), 0, sizeof(L_229));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_229), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_228);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_228, L_229);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_230 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_230, 1));
	}

IL_0901:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_231 = V_15;
		int32_t L_232 = __this->___scr_lvl_21;
		if ((((int32_t)L_231) <= ((int32_t)L_232)))
		{
			goto IL_08cf;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_233 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_233, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_234 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_234, NULL);
		// money = money - 650;
		float L_235 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_235, (650.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_236 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_236, NULL);
		// scr_txt.text = "$700";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_237 = __this->___scr_txt_23;
		NullCheck(L_237);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_237, _stringLiteral47CDF301842434784DCD8A8EE0240C60A86C04F4);
		return;
	}

IL_094a:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_238 = __this->___warningpanal_33;
		NullCheck(L_238);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_238, (bool)1, NULL);
		// break;
		return;
	}

IL_0957:
	{
		// if (money >= 700)
		float L_239 = V_1;
		if ((!(((float)L_239) >= ((float)(700.0f)))))
		{
			goto IL_09f8;
		}
	}
	{
		// power = power + 0.25f;
		float L_240 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_240, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_241 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_241, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_16 = 0;
		goto IL_09af;
	}

IL_097d:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_242 = __this->___lvl_scr_22;
		int32_t L_243 = V_16;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_246;
		L_246 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_245, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_247;
		memset((&L_247), 0, sizeof(L_247));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_247), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_246);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_246, L_247);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_248 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_248, 1));
	}

IL_09af:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_249 = V_16;
		int32_t L_250 = __this->___scr_lvl_21;
		if ((((int32_t)L_249) <= ((int32_t)L_250)))
		{
			goto IL_097d;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_251 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_251, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_252 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_252, NULL);
		// money = money - 700;
		float L_253 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_253, (700.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_254 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_254, NULL);
		// scr_txt.text = "$750";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_255 = __this->___scr_txt_23;
		NullCheck(L_255);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_255, _stringLiteral14776F72367EBE659742DE93EDBEE96CD7027CEF);
		return;
	}

IL_09f8:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_256 = __this->___warningpanal_33;
		NullCheck(L_256);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_256, (bool)1, NULL);
		// break;
		return;
	}

IL_0a05:
	{
		// if (money >= 750)
		float L_257 = V_1;
		if ((!(((float)L_257) >= ((float)(750.0f)))))
		{
			goto IL_0aa6;
		}
	}
	{
		// power = power + 0.25f;
		float L_258 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_258, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_259 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_259, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_17 = 0;
		goto IL_0a5d;
	}

IL_0a2b:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_260 = __this->___lvl_scr_22;
		int32_t L_261 = V_17;
		NullCheck(L_260);
		int32_t L_262 = L_261;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_263 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_262));
		NullCheck(L_263);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_264;
		L_264 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_263, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_265;
		memset((&L_265), 0, sizeof(L_265));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_265), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_264);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_264, L_265);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_266 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_266, 1));
	}

IL_0a5d:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_267 = V_17;
		int32_t L_268 = __this->___scr_lvl_21;
		if ((((int32_t)L_267) <= ((int32_t)L_268)))
		{
			goto IL_0a2b;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_269 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_269, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_270 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_270, NULL);
		// money = money - 750;
		float L_271 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_271, (750.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_272 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_272, NULL);
		// scr_txt.text = "$800";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_273 = __this->___scr_txt_23;
		NullCheck(L_273);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_273, _stringLiteral60E7EAEF5809BF0BBE2CA2002E49DF68C2317F98);
		return;
	}

IL_0aa6:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_274 = __this->___warningpanal_33;
		NullCheck(L_274);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_274, (bool)1, NULL);
		// break;
		return;
	}

IL_0ab3:
	{
		// if (money >= 800)
		float L_275 = V_1;
		if ((!(((float)L_275) >= ((float)(800.0f)))))
		{
			goto IL_0b54;
		}
	}
	{
		// power = power + 0.25f;
		float L_276 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_276, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_277 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_277, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_18 = 0;
		goto IL_0b0b;
	}

IL_0ad9:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_278 = __this->___lvl_scr_22;
		int32_t L_279 = V_18;
		NullCheck(L_278);
		int32_t L_280 = L_279;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_281 = (L_278)->GetAt(static_cast<il2cpp_array_size_t>(L_280));
		NullCheck(L_281);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_282;
		L_282 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_281, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_283;
		memset((&L_283), 0, sizeof(L_283));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_283), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_282, L_283);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_284 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add(L_284, 1));
	}

IL_0b0b:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_285 = V_18;
		int32_t L_286 = __this->___scr_lvl_21;
		if ((((int32_t)L_285) <= ((int32_t)L_286)))
		{
			goto IL_0ad9;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_287 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_287, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_288 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_288, NULL);
		// money = money - 800;
		float L_289 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_289, (800.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_290 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_290, NULL);
		// scr_txt.text = "$850";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_291 = __this->___scr_txt_23;
		NullCheck(L_291);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_291, _stringLiteral7518D02362407CA28801E1665AD6EF1FA9976309);
		return;
	}

IL_0b54:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_292 = __this->___warningpanal_33;
		NullCheck(L_292);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_292, (bool)1, NULL);
		// break;
		return;
	}

IL_0b61:
	{
		// if (money >= 850)
		float L_293 = V_1;
		if ((!(((float)L_293) >= ((float)(850.0f)))))
		{
			goto IL_0c02;
		}
	}
	{
		// power = power + 0.25f;
		float L_294 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_294, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_295 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_295, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_19 = 0;
		goto IL_0bb9;
	}

IL_0b87:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_296 = __this->___lvl_scr_22;
		int32_t L_297 = V_19;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_300;
		L_300 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_299, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_301;
		memset((&L_301), 0, sizeof(L_301));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_301), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_300, L_301);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_302 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_302, 1));
	}

IL_0bb9:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_303 = V_19;
		int32_t L_304 = __this->___scr_lvl_21;
		if ((((int32_t)L_303) <= ((int32_t)L_304)))
		{
			goto IL_0b87;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_305 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_305, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_306 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_306, NULL);
		// money = money - 850;
		float L_307 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_307, (850.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_308 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_308, NULL);
		// scr_txt.text = "$900";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_309 = __this->___scr_txt_23;
		NullCheck(L_309);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_309, _stringLiteral2FB9AFC3C6E1E4AB4F5AEF3D6ED19C3E67FEA800);
		return;
	}

IL_0c02:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_310 = __this->___warningpanal_33;
		NullCheck(L_310);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_310, (bool)1, NULL);
		// break;
		return;
	}

IL_0c0f:
	{
		// if (money >= 900)
		float L_311 = V_1;
		if ((!(((float)L_311) >= ((float)(900.0f)))))
		{
			goto IL_0cb0;
		}
	}
	{
		// power = power + 0.25f;
		float L_312 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_312, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_313 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_313, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_20 = 0;
		goto IL_0c67;
	}

IL_0c35:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_314 = __this->___lvl_scr_22;
		int32_t L_315 = V_20;
		NullCheck(L_314);
		int32_t L_316 = L_315;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_317 = (L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316));
		NullCheck(L_317);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_318;
		L_318 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_317, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_319;
		memset((&L_319), 0, sizeof(L_319));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_319), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_318);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_318, L_319);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_320 = V_20;
		V_20 = ((int32_t)il2cpp_codegen_add(L_320, 1));
	}

IL_0c67:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_321 = V_20;
		int32_t L_322 = __this->___scr_lvl_21;
		if ((((int32_t)L_321) <= ((int32_t)L_322)))
		{
			goto IL_0c35;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_323 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_323, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_324 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_324, NULL);
		// money = money - 900;
		float L_325 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_325, (900.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_326 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_326, NULL);
		// scr_txt.text = "950";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_327 = __this->___scr_txt_23;
		NullCheck(L_327);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_327, _stringLiteral14E7449F16302DF2516E1B490CA9947FE3C94A55);
		return;
	}

IL_0cb0:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_328 = __this->___warningpanal_33;
		NullCheck(L_328);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_328, (bool)1, NULL);
		// break;
		return;
	}

IL_0cbd:
	{
		// if (money >= 900)
		float L_329 = V_1;
		if ((!(((float)L_329) >= ((float)(900.0f)))))
		{
			goto IL_0d5e;
		}
	}
	{
		// power = power + 0.25f;
		float L_330 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_330, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_331 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_331, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_21 = 0;
		goto IL_0d15;
	}

IL_0ce3:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_332 = __this->___lvl_scr_22;
		int32_t L_333 = V_21;
		NullCheck(L_332);
		int32_t L_334 = L_333;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_335 = (L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334));
		NullCheck(L_335);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_336;
		L_336 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_335, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_337;
		memset((&L_337), 0, sizeof(L_337));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_337), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_336);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_336, L_337);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_338 = V_21;
		V_21 = ((int32_t)il2cpp_codegen_add(L_338, 1));
	}

IL_0d15:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_339 = V_21;
		int32_t L_340 = __this->___scr_lvl_21;
		if ((((int32_t)L_339) <= ((int32_t)L_340)))
		{
			goto IL_0ce3;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_341 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_341, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_342 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_342, NULL);
		// money = money - 950;
		float L_343 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_343, (950.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_344 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_344, NULL);
		// scr_txt.text = "1000";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_345 = __this->___scr_txt_23;
		NullCheck(L_345);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_345, _stringLiteral8739AB73E27D444291AAFE563DFBA8832AD5D722);
		return;
	}

IL_0d5e:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_346 = __this->___warningpanal_33;
		NullCheck(L_346);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_346, (bool)1, NULL);
		// break;
		return;
	}

IL_0d6b:
	{
		// if (money >= 1000)
		float L_347 = V_1;
		if ((!(((float)L_347) >= ((float)(1000.0f)))))
		{
			goto IL_0e0c;
		}
	}
	{
		// power = power + 0.25f;
		float L_348 = V_0;
		V_0 = ((float)il2cpp_codegen_add(L_348, (0.25f)));
		// scr_lvl = scr_lvl + 1;
		int32_t L_349 = __this->___scr_lvl_21;
		__this->___scr_lvl_21 = ((int32_t)il2cpp_codegen_add(L_349, 1));
		// for (int i = 0; i <= scr_lvl; i++)
		V_22 = 0;
		goto IL_0dc3;
	}

IL_0d91:
	{
		// lvl_scr[i].GetComponent<Image>().color = new Color(255, 238, 27, 255);
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_350 = __this->___lvl_scr_22;
		int32_t L_351 = V_22;
		NullCheck(L_350);
		int32_t L_352 = L_351;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_353 = (L_350)->GetAt(static_cast<il2cpp_array_size_t>(L_352));
		NullCheck(L_353);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_354;
		L_354 = GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D(L_353, GameObject_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mA59EA7D5F9133B2593F4AB70B099928BA955EE7D_RuntimeMethod_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_355;
		memset((&L_355), 0, sizeof(L_355));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_355), (255.0f), (238.0f), (27.0f), (255.0f), /*hidden argument*/NULL);
		NullCheck(L_354);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_354, L_355);
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_356 = V_22;
		V_22 = ((int32_t)il2cpp_codegen_add(L_356, 1));
	}

IL_0dc3:
	{
		// for (int i = 0; i <= scr_lvl; i++)
		int32_t L_357 = V_22;
		int32_t L_358 = __this->___scr_lvl_21;
		if ((((int32_t)L_357) <= ((int32_t)L_358)))
		{
			goto IL_0d91;
		}
	}
	{
		// PlayerPrefs.SetFloat("scr_pow", power);
		float L_359 = V_0;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteralD59DCCD92EB58BAFFF2F13237E0CD74D8FA07733, L_359, NULL);
		// PlayerPrefs.SetInt("scr_lvl", scr_lvl);
		int32_t L_360 = __this->___scr_lvl_21;
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral8F93A0D28518F610F441656C0E156FC1120134AD, L_360, NULL);
		// money = money - 1000;
		float L_361 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_361, (1000.0f)));
		// PlayerPrefs.SetFloat("money", money);
		float L_362 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral88B8A873F47E7F0762D303AD565BFFC23FE279BC, L_362, NULL);
		// scr_txt.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_363 = __this->___scr_txt_23;
		NullCheck(L_363);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_363, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		return;
	}

IL_0e0c:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_364 = __this->___warningpanal_33;
		NullCheck(L_364);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_364, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void up_manager::homing_missile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_homing_missile_m720128A724E65BC0C2F4AA659DE04957807F49A4 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// bool homing = System.Convert.ToBoolean(PlayerPrefs.GetInt("homing"));
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_0, NULL);
		V_0 = L_1;
		// float money = PlayerPrefs.GetFloat("gem");
		float L_2;
		L_2 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, NULL);
		V_1 = L_2;
		// if (!PlayerPrefs.HasKey("homing"))
		bool L_3;
		L_3 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1, NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		// homing = false;
		V_0 = (bool)0;
	}

IL_0029:
	{
		// if (money >= 500)
		float L_4 = V_1;
		if ((!(((float)L_4) >= ((float)(500.0f)))))
		{
			goto IL_0068;
		}
	}
	{
		// if (homing == false)
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		// money = money - 500;
		float L_6 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_6, (500.0f)));
		// PlayerPrefs.SetFloat("gem", money);
		float L_7 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, L_7, NULL);
		// homing_txt.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_8 = __this->___homing_txt_24;
		NullCheck(L_8);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		// PlayerPrefs.SetInt("homing", System.Convert.ToInt32(true));
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = Convert_ToInt32_m54E891D837D238AD3700E7554AA565E69A7BC983((bool)1, NULL);
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteralF957DF8149890A731A4F644896BC1D7CC38EF3E1, L_9, NULL);
		return;
	}

IL_0068:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___warningpanal_33;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// System.Void up_manager::splitifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_splitifier_m612B0C99D7E45BC5142F633A23ABD78A336F5EFC (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// bool homing = System.Convert.ToBoolean(PlayerPrefs.GetInt("splitifier"));
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_0, NULL);
		V_0 = L_1;
		// float money = PlayerPrefs.GetFloat("gem");
		float L_2;
		L_2 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, NULL);
		V_1 = L_2;
		// if (!PlayerPrefs.HasKey("splitifier"))
		bool L_3;
		L_3 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF, NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		// homing = false;
		V_0 = (bool)0;
	}

IL_0029:
	{
		// if (money >= 1500)
		float L_4 = V_1;
		if ((!(((float)L_4) >= ((float)(1500.0f)))))
		{
			goto IL_0068;
		}
	}
	{
		// if (homing == false)
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		// money = money - 1500;
		float L_6 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_6, (1500.0f)));
		// PlayerPrefs.SetFloat("gem", money);
		float L_7 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, L_7, NULL);
		// splt_text.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_8 = __this->___splt_text_25;
		NullCheck(L_8);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		// PlayerPrefs.SetInt("splitifier", System.Convert.ToInt32(true));
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = Convert_ToInt32_m54E891D837D238AD3700E7554AA565E69A7BC983((bool)1, NULL);
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteralF463A12EF41593016707B6BCBC754FD6D5A645CF, L_9, NULL);
		return;
	}

IL_0068:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___warningpanal_33;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// System.Void up_manager::crusher()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_crusher_m25F8AD7F31E61AA22F4B55BD812021D61845996B (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABBE9CBE1EFB1B0D5CE746178D6D006A481E03A9);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// bool homing = System.Convert.ToBoolean(PlayerPrefs.GetInt("spike_crush"));
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteralABBE9CBE1EFB1B0D5CE746178D6D006A481E03A9, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_0, NULL);
		V_0 = L_1;
		// float money = PlayerPrefs.GetFloat("gem");
		float L_2;
		L_2 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, NULL);
		V_1 = L_2;
		// if (!PlayerPrefs.HasKey("spike_crush"))
		bool L_3;
		L_3 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteralABBE9CBE1EFB1B0D5CE746178D6D006A481E03A9, NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		// homing = false;
		V_0 = (bool)0;
	}

IL_0029:
	{
		// if (money >= 1000)
		float L_4 = V_1;
		if ((!(((float)L_4) >= ((float)(1000.0f)))))
		{
			goto IL_0068;
		}
	}
	{
		// if (homing == false)
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		// money = money - 1000;
		float L_6 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_6, (1000.0f)));
		// PlayerPrefs.SetFloat("gem", money);
		float L_7 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, L_7, NULL);
		// spike_txt.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_8 = __this->___spike_txt_26;
		NullCheck(L_8);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		// PlayerPrefs.SetInt("spike_crush", System.Convert.ToInt32(true));
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = Convert_ToInt32_m54E891D837D238AD3700E7554AA565E69A7BC983((bool)1, NULL);
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteralABBE9CBE1EFB1B0D5CE746178D6D006A481E03A9, L_9, NULL);
		return;
	}

IL_0068:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___warningpanal_33;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// System.Void up_manager::hyperbeam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_hyperbeam_m709F3EA3D3EC55E3D12A8B0F28F871E273608093 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// bool homing = System.Convert.ToBoolean(PlayerPrefs.GetInt("hyperbeam"));
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m4D859DBEABAD3FB406C94485A0B2638A0C7F2987(_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74, NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_0, NULL);
		V_0 = L_1;
		// float money = PlayerPrefs.GetFloat("gem");
		float L_2;
		L_2 = PlayerPrefs_GetFloat_m81F89D571E11218ED76DC9234CF8FAC2515FA7CB(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, NULL);
		V_1 = L_2;
		// if (!PlayerPrefs.HasKey("hyperbeam"))
		bool L_3;
		L_3 = PlayerPrefs_HasKey_mCA5C64BBA6BF8B230BC3BC92B4761DD3B11D4668(_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74, NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		// homing = false;
		V_0 = (bool)0;
	}

IL_0029:
	{
		// if (money >= 3000)
		float L_4 = V_1;
		if ((!(((float)L_4) >= ((float)(3000.0f)))))
		{
			goto IL_0068;
		}
	}
	{
		// if (homing == false)
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		// money = money - 3000;
		float L_6 = V_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_6, (3000.0f)));
		// PlayerPrefs.SetFloat("gem", money);
		float L_7 = V_1;
		PlayerPrefs_SetFloat_m1E8EA662BB9D8CF339D7DA2C452FCDFED88C5285(_stringLiteral71A0AB63247A5E94C595BDE4C2F32D4F8EE7644F, L_7, NULL);
		// hyper_txt.text = "Sold";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_8 = __this->___hyper_txt_27;
		NullCheck(L_8);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, _stringLiteral458DF341DB7EEB2D85B3BD596E2EFFF33089A5F1);
		// PlayerPrefs.SetInt("hyperbeam", System.Convert.ToInt32(true));
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = Convert_ToInt32_m54E891D837D238AD3700E7554AA565E69A7BC983((bool)1, NULL);
		PlayerPrefs_SetInt_m956D3E2DB966F20CF42F842880DDF9E2BE94D948(_stringLiteral610983DC0468171E2C3A77615283FAF3CB36DC74, L_9, NULL);
		return;
	}

IL_0068:
	{
		// warningpanal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___warningpanal_33;
		NullCheck(L_10);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_10, (bool)1, NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// System.Void up_manager::buy_paqnal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_buy_paqnal_mE4082BD791C465540CAAB625E135A2EEE2363299 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// buy_panal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___buy_panal_28;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// upgrade_panal.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___upgrade_panal_29;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// slider.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___slider_30;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// buy.GetComponent<Image>().color = new Color32(80, 60 , 108, 255);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_3 = __this->___buy_32;
		NullCheck(L_3);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_4;
		L_4 = Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79(L_3, Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5;
		memset((&L_5), 0, sizeof(L_5));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_5), (uint8_t)((int32_t)80), (uint8_t)((int32_t)60), (uint8_t)((int32_t)108), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6;
		L_6 = Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_6);
		// upgrade.GetComponent<Image>().color = new Color32(102, 13, 221, 255);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_7 = __this->___upgrade_31;
		NullCheck(L_7);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_8;
		L_8 = Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79(L_7, Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_9;
		memset((&L_9), 0, sizeof(L_9));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_9), (uint8_t)((int32_t)102), (uint8_t)((int32_t)13), (uint8_t)((int32_t)221), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10;
		L_10 = Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline(L_9, NULL);
		NullCheck(L_8);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_10);
		// }
		return;
	}
}
// System.Void up_manager::upgrade_paqal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_upgrade_paqal_m0CF427F87CBEBF1FB47A5AA660A472C03E2BF8E2 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// buy_panal.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___buy_panal_28;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// upgrade_panal.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___upgrade_panal_29;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// slider.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___slider_30;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)1, NULL);
		// upgrade.GetComponent<Image>().color = new Color32(80, 60, 108, 255);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_3 = __this->___upgrade_31;
		NullCheck(L_3);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_4;
		L_4 = Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79(L_3, Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5;
		memset((&L_5), 0, sizeof(L_5));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_5), (uint8_t)((int32_t)80), (uint8_t)((int32_t)60), (uint8_t)((int32_t)108), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6;
		L_6 = Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline(L_5, NULL);
		NullCheck(L_4);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_6);
		// buy.GetComponent<Image>().color = new Color32(102, 13, 221, 255);
		Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* L_7 = __this->___buy_32;
		NullCheck(L_7);
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_8;
		L_8 = Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79(L_7, Component_GetComponent_TisImage_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_mE74EE63C85A63FC34DCFC631BC229207B420BC79_RuntimeMethod_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_9;
		memset((&L_9), 0, sizeof(L_9));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_9), (uint8_t)((int32_t)102), (uint8_t)((int32_t)13), (uint8_t)((int32_t)221), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10;
		L_10 = Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline(L_9, NULL);
		NullCheck(L_8);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_10);
		// }
		return;
	}
}
// System.Void up_manager::cross()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager_cross_mF34EE0715C48E97AC1BF7C76B87D001DF80640B4 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	{
		// warningpanal.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___warningpanal_33;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void up_manager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void up_manager__ctor_m970770C3C1A6B8C0373292370F58443EE80998B9 (up_manager_t138DD9E97688DBE7F6FAD846670BC5D97D05EBFD* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		float L_3 = ___3_a;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___0_r, uint8_t ___1_g, uint8_t ___2_b, uint8_t ___3_a, const RuntimeMethod* method) 
{
	{
		__this->___rgba_0 = 0;
		uint8_t L_0 = ___0_r;
		__this->___r_1 = L_0;
		uint8_t L_1 = ___1_g;
		__this->___g_2 = L_1;
		uint8_t L_2 = ___2_b;
		__this->___b_3 = L_2;
		uint8_t L_3 = ___3_a;
		__this->___a_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m47CBB138122B400E0B1F4BFD7C30A6C2C00FCA3E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___0_c, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = ___0_c;
		uint8_t L_1 = L_0.___r_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_2 = ___0_c;
		uint8_t L_3 = L_2.___g_2;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_4 = ___0_c;
		uint8_t L_5 = L_4.___b_3;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6 = ___0_c;
		uint8_t L_7 = L_6.___a_4;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8;
		memset((&L_8), 0, sizeof(L_8));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_8), ((float)(((float)L_1)/(255.0f))), ((float)(((float)L_3)/(255.0f))), ((float)(((float)L_5)/(255.0f))), ((float)(((float)L_7)/(255.0f))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_003d;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = V_0;
		return L_9;
	}
}
